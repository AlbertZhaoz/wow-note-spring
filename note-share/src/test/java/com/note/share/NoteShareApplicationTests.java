package com.note.share;

import com.note.share.util.RedisUtils;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.concurrent.TimeUnit;

@SpringBootTest
class NoteShareApplicationTests {

    @Test
    void contextLoads() {
        RedisUtils.save("123", String.format("username:%s", "456"), 1L, TimeUnit.MINUTES);

    }

}
