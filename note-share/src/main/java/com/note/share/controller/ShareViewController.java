package com.note.share.controller;

import com.note.share.enums.ResponseEnum;
import com.note.share.service.ShareService;
import com.note.share.vo.ResponseVo;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Package：com.note.share.controller
 * @Name：ShareViewController
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2024-02-03-12:31
 * @Description：分享链接查看控制类
 */
@Validated
@RestController
@RequestMapping("/view")
@Tag(name = "ShareViewController", description = "分享链接预览控制类")
public class ShareViewController {

    @Autowired
    public ShareService shareService;

    @GetMapping({"/{shareId}"})
    public ResponseVo viewShare(@PathVariable("shareId") String shareId) {
        return new ResponseVo(ResponseEnum.SUCCESS, shareService.viewShare(shareId));
    }
}
