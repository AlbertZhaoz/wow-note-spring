package com.note.share.controller;

import com.note.share.constants.ResponseCodeConstants;
import com.note.share.dto.ShareDto;
import com.note.share.dto.ShareModifyDto;
import com.note.share.enums.ResponseEnum;
import com.note.share.service.ShareService;
import com.note.share.vo.ResponseVo;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.constraints.NotBlank;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @Package：com.note.share.controller
 * @Name：ShareController
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2024-02-02-15:06
 * @Description：文档或文件夹分享控制类
 */

@Slf4j
@Validated
@RestController
@RequestMapping("/share")
@Tag(name = "ShareController", description = "分享笔记或目录相关接口")
public class ShareController {

    @Autowired
    private ShareService shareService;

    @ResponseBody
    @PostMapping("/create")
    @Operation(summary = "createNewShare", description = "创建新的分享（文件夹编号folderId|笔记编号noteId|分享结束时间endTime）")
    public ResponseVo createNewShare(@RequestBody @Validated({ShareService.class}) ShareDto shareDto) {
        int resCode = shareService.createShare(shareDto);
        return new ResponseVo(ResponseEnum.SHARE_SUCCESS);
    }

    @ResponseBody
    @PutMapping("/modify")
    @Operation(summary = "modifyShare", description = "修改已有的分享（分享编号shareId|分享状态type）")
    public ResponseVo modifyShare(@RequestBody @Validated({ShareService.class}) ShareModifyDto shareModifyDto) {
        int reCode = shareService.modifyShare(shareModifyDto);
        return switch (reCode) {
            case ResponseCodeConstants.NO_SHARE_RECORD -> new ResponseVo(ResponseEnum.NO_SHARE_RECORD);
            default -> new ResponseVo<>(ResponseEnum.CHANGE_SHARE_STATUS_SUCCESS);
        };
    }

    @ResponseBody
    @DeleteMapping("/delete")
    @Operation(summary = "deleteShare", description = "删除已有的分享（分享编号shareId）")
    public ResponseVo deleteShare(@RequestParam @NotBlank(message = "分享ID不能为空") String shareId) {
        int reCode = shareService.removeShare(shareId);
        return switch (reCode) {
            case ResponseCodeConstants.NO_SHARE_RECORD -> new ResponseVo(ResponseEnum.NO_SHARE_RECORD);
            default -> new ResponseVo<>(ResponseEnum.DELETE_SUCCESS);
        };
    }

    @ResponseBody
    @GetMapping("/shares")
    @Operation(summary = "getAllShare", description = "获取所有分享")
    public ResponseVo getAllShare(@RequestParam @NotBlank(message = "用户名不能为空") String username) {
        return new ResponseVo(ResponseEnum.SUCCESS, shareService.getAllShare(username));
    }
}
