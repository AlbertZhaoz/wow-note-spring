package com.note.share.dao.impl;

import com.note.config.constants.domain.Note;
import com.note.share.constants.CollectionNameConstants;
import com.note.share.dao.ShareRepository;
import com.note.share.domain.Share;
import com.note.share.util.RedisUtils;
import com.note.share.util.TimeUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @Package：com.note.share.dao.impl
 * @Name：ShareRepositoryImpl
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2024-02-02-17:42
 * @Description：笔记和目录分享业务数据库操作实现
 */
@Slf4j
@Repository
public class ShareRepositoryImpl implements ShareRepository {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public void insertShare(Share share) {
        mongoTemplate.insert(share, CollectionNameConstants.SHARE);
        //记录过期时间到Redis
        log.error("TimeUtils.getDateDay(share.getEndTime()):{}", TimeUtils.getDateDay(share.getEndTime()));
        RedisUtils.save(share.getId(), String.format("username:%s", share.getNoteId()), TimeUtils.getDateDay(share.getEndTime()), TimeUnit.DAYS);
    }

    @Override
    public void updateShare(String shareId, int type) {
        /*这里直接 id 即可，因为下面updateFist方法给定了文档的模型*/
        Query query = new Query(Criteria.where("id").is(shareId));//条件定义
        Update update = new Update();//更新定义
        update.set("type", type);
        mongoTemplate.updateFirst(query, update, Share.class);
    }

    @Override
    public void deleteShare(String shareId) {
        Query query = new Query(Criteria.where("id").is(shareId));//条件定义
        //移除Redis中的记录
        RedisUtils.remove(shareId);
        mongoTemplate.remove(query, Share.class);
    }

    @Override
    public List<Share> selectShares(String username) {
        Query query = new Query(Criteria.where("userId").is(username));//条件定义
        return mongoTemplate.find(query, Share.class);
    }

    @Override
    public boolean existsShare(String shareId) {
        Query query = new Query(Criteria.where("id").is(shareId));//条件定义
        return mongoTemplate.exists(query, Share.class);
    }

    @Override
    public Share getShare(String shareId) {
        Query query = new Query(Criteria.where("id").is(shareId));
        return mongoTemplate.findOne(query, Share.class);
    }

    @Override
    public boolean isSharedNote(String noteId) {
        Query query = new Query(Criteria.where("note_id").is(noteId));
        return mongoTemplate.exists(query, CollectionNameConstants.SHARE);
    }

    @Override
    public Note selectShareNoteName(String noteId) {
        Query query = new Query(Criteria.where("id").is(noteId));
        return mongoTemplate.findOne(query, Note.class);

    }
}
