package com.note.share.dao;

import com.note.config.constants.domain.Note;
import com.note.share.domain.Share;

import java.util.List;

/**
 * @Package：com.note.share.dao
 * @Name：ShareRepository
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2024-02-02-17:42
 * @Description：笔记和目录分享业务数据库操作接口
 */
public interface ShareRepository {

    /**
     * 创建新的分享
     *
     * @param share
     * @return
     */
    void insertShare(Share share);

    /**
     * 更新分享状态
     *
     * @param shareId
     * @param type
     */
    void updateShare(String shareId, int type);

    /**
     * 删除分享
     *
     * @param shareId
     * @return
     */
    void deleteShare(String shareId);

    /**
     * 获取分享
     *
     * @param username
     * @return
     */
    List<Share> selectShares(String username);

    /**
     * 判断分享是否存在
     *
     * @param shareId
     * @return
     */
    boolean existsShare(String shareId);

    /**
     * 获取Share
     *
     * @param shareId
     * @return
     */
    Share getShare(String shareId);

    /**
     * 查询一个文档是否存在·
     *
     * @param noteId
     * @return Note
     */
    boolean isSharedNote(String noteId);

    /**
     * 查询笔记
     *
     * @param noteId
     * @return Note
     */
    Note selectShareNoteName(String noteId);
}
