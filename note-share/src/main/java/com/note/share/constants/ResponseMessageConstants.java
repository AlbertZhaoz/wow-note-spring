package com.note.share.constants;

/**
 * @Package：com.note.share.constants
 * @Name：ReponseMessageConstants
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2024-02-02-17:57
 * @Description：错误消息结果定义
 */
public class ResponseMessageConstants {
    /*分享相关错误消息*/
    public static final String SHARE_FAILED = "分享失败";
    public static final String SHARE_SUCCESS = "分享成功";
    public static final String DELETE_SHARE_FAILED = "删除分享失败";
    public static final String CHANGE_SHARE_STATUS_SUCCESS = "更改分享状态成功";
    public static final String NO_SHARE_RECORD = "暂无分享记录";

    public static final String DELETE_SUCCESS = "取消分享成功";

}