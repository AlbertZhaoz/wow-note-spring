package com.note.share.constants;

/**
 * @Package：com.note.share.constants
 * @Name：ResponseCodeConstants
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2024-02-02-17:56
 * @Description：错误消息状态码定义
 */
public class ResponseCodeConstants {
    /*分享相关错误状态码 范围：1001-1050*/
    public static final int SHARE_FAILED = 1001;
    public static final int DELETE_SHARE_FAILED = 1002;
    public static final int CHANGE_SHARE_STATUS_SUCCESS = 1003;
    public static final int NO_SHARE_RECORD = 1004;
    public static final int DELETE_SUCCESS = 1005;
    public static final int SHARE_SUCCESS = 1006;
}
