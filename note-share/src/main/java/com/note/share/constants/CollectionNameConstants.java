package com.note.share.constants;

/**
 * @Package：com.note.share.constants
 * @Name：CollectionNameConstants
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2024-02-02-17:54
 * @Description：集合的文档名常量
 */
public class CollectionNameConstants {
    public static final String SHARE  = "share";
}
