package com.note.share.enums;

import com.note.share.constants.ResponseCodeConstants;
import com.note.share.constants.ResponseMessageConstants;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Getter;

/**
 * @Package：com.note.share.enums
 * @Name：ResponseEnum
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2024-02-02-18:02
 * @Description：统一返回枚举类
 */
@Getter
@Tag(name = "ResponseEnum", description = "分享模块统一返回枚举类")
public enum ResponseEnum {
    /*默认状态*/
    SUCCESS(200, true, "请求成功"),
    FAIL(400, false, "请求失败"),

    /*分享相关错误状态*/
    SHARE_FIELD(ResponseCodeConstants.SHARE_FAILED, false, ResponseMessageConstants.SHARE_FAILED),
    DELETE_SHARE_FILED(ResponseCodeConstants.DELETE_SHARE_FAILED, false, ResponseMessageConstants.DELETE_SHARE_FAILED),
    CHANGE_SHARE_STATUS_SUCCESS(ResponseCodeConstants.CHANGE_SHARE_STATUS_SUCCESS, true, ResponseMessageConstants.CHANGE_SHARE_STATUS_SUCCESS),
    NO_SHARE_RECORD(ResponseCodeConstants.NO_SHARE_RECORD, false, ResponseMessageConstants.NO_SHARE_RECORD),
    DELETE_SUCCESS(ResponseCodeConstants.DELETE_SUCCESS, true, ResponseMessageConstants.DELETE_SUCCESS),
    SHARE_SUCCESS(ResponseCodeConstants.SHARE_SUCCESS, true, ResponseMessageConstants.SHARE_SUCCESS);


    /*状态码*/
    private final int code;
    /*请求状态*/
    private final boolean status;
    /*请求结果*/
    private final String message;

    ResponseEnum(int code, boolean status, String message) {
        this.code = code;
        this.status = status;
        this.message = message;
    }
}
