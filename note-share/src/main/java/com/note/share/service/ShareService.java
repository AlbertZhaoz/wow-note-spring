package com.note.share.service;

import com.note.share.domain.Share;
import com.note.share.dto.ShareDto;
import com.note.share.dto.ShareModifyDto;

import java.util.List;

/**
 * @Package：com.note.share.service.impl
 * @Name：ShareService
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2024-02-02-17:38
 * @Description：笔记和目录分操作接口享
 */
public interface ShareService {

    /**
     * 创建新的分享
     *
     * @param shareDto
     * @return
     */
    int createShare(ShareDto shareDto);

    /**
     * 更新分享状态
     *
     * @param shareModifyDto
     * @return
     */
    int modifyShare(ShareModifyDto shareModifyDto);

    /**
     * 删除分享
     *
     * @param shareId
     * @return
     */
    int removeShare(String shareId);

    /**
     * 获取分享
     *
     * @param username
     * @return
     */
    List<Share> getAllShare(String username);

    /**
     * 获取Share
     *
     * @param shareId
     * @return
     */
    Share getShare(String shareId);

    /**
     * 查看分享
     *
     * @param shareID
     * @return
     */
    Share viewShare(String shareID);
}
