package com.note.share.service.impl;

import cn.hutool.core.lang.ObjectId;
import com.note.share.constants.ResponseCodeConstants;
import com.note.share.dao.ShareRepository;
import com.note.share.domain.Share;
import com.note.share.dto.ShareDto;
import com.note.share.dto.ShareModifyDto;
import com.note.share.service.ShareService;
import com.note.share.util.TimeUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * @Package：com.note.share.service.impl
 * @Name：ShareServiceImpl
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2024-02-02-17:39
 * @Description：笔记或目录分享接口实现
 */
@Slf4j
@Service
public class ShareServiceImpl implements ShareService {
    @Autowired
    private ShareRepository shareRepository;

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public int createShare(ShareDto shareDto) {
        String folderId = shareDto.getFolderId();
        String noteId = shareDto.getNoteId();
        if (!"0".equals(folderId)) {
            throw new RuntimeException("暂无支持文件夹分享");
        }
        if (!TimeUtils.isNDays(shareDto.getEndTime(), 31L)) {
            throw new RuntimeException("设定时间错误,请重试");
        }
        //判断是否已经存在该笔记
        if (shareRepository.isSharedNote(shareDto.getNoteId())) {
            throw new RuntimeException("该笔记已分享,无法重复");
        }
        if (isaBoolean(folderId, noteId)) {
            Share share = new Share();
            share.setId(ObjectId.next());
            share.setType(1);/*初次正常*/
            share.setEndTime(shareDto.getEndTime());
            share.setFolderId(folderId);
            share.setNoteId(noteId);
            share.setUserId(shareDto.getUsername());
            shareRepository.insertShare(share);
            return ResponseCodeConstants.SHARE_SUCCESS;
        }
        throw new RuntimeException("参数有误,请重试");
    }

    /*笔记和目录ID验证*/
    private static boolean isaBoolean(String folderId, String noteId) {
        //暂时不写支持分享文件夹
        return (!"0".equals(folderId) && "0".equals(noteId)) || ("0".equals(folderId) && !"0".equals(noteId));
    }

    @Override
    public int modifyShare(ShareModifyDto shareModifyDto) {
        if (!shareRepository.existsShare(shareModifyDto.getShareId())) {
            return ResponseCodeConstants.NO_SHARE_RECORD;
        }
        shareRepository.updateShare(shareModifyDto.getShareId(), shareModifyDto.getType());
        return ResponseCodeConstants.CHANGE_SHARE_STATUS_SUCCESS;
    }

    @Override
    public int removeShare(String shareId) {
        if (!shareRepository.existsShare(shareId)) return ResponseCodeConstants.NO_SHARE_RECORD;
        shareRepository.deleteShare(shareId);
        return ResponseCodeConstants.DELETE_SUCCESS;
    }

    @Override
    public List<Share> getAllShare(String username) {
        List<Share> shareList = shareRepository.selectShares(username);
        //插入标题
        shareList.forEach(share -> {
            share.setTitle(shareRepository.selectShareNoteName(share.getNoteId()).getTitle());
        });
        return shareList;
    }

    @Override
    public Share getShare(String shareId) {
        if (!shareRepository.existsShare(shareId)) {
            throw new RuntimeException("该分享不存在");
        }
        return shareRepository.getShare(shareId);
    }

    @Override
    public Share viewShare(String shareID) {
        Share share = getShare(shareID);
        if (share.getType() == 0) {
            throw new RuntimeException("暂无权限查看");
        }
        if (isNote(share.getFolderId(), share.getNoteId())) {
            //文件夹分享
            throw new RuntimeException("暂无支持文件夹分享");
        } else {
            //笔记分享
            return share;
        }
    }

    /*判断是分享笔记还是目录*/
    private static boolean isNote(String folderId, String noteId) {
        //log.error("F:{},N:{}", folderId, noteId);
        if ("0".equals(folderId)) {
            return false;
        } else if ("0".equals(noteId)) {
            return true;
        } else {
            throw new RuntimeException("错误分享，请重试");
        }
    }
}
