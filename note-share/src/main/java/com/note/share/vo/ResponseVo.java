package com.note.share.vo;

import com.note.share.enums.ResponseEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Data;

import java.io.Serializable;

/**
 * @Package：com.note.share.vo
 * @Name：ResponseVo
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2024-02-02-18:18
 * @Description：全局统一返回类
 */


@Data
@Tag(name = "ResponseVo", description = "统一返回处理")
public class ResponseVo<T> implements Serializable {
    @Schema(name = "code", description = "状态码")
    private int code;
    @Schema(name = "status", description = "请求状态")
    private boolean status;
    @Schema(name = "message", description = "请求结果")
    private String message;
    @Schema(name = "data", description = "状态数据")
    private T data;

    /*全参构造*/
    public ResponseVo(int code, boolean status, String message, T data) {
        this.code = code;
        this.status = status;
        this.message = message;
        this.data = data;
    }

    /*无data构造*/
    public ResponseVo(int code, boolean status, String message) {
        this.code = code;
        this.status = status;
        this.message = message;
        this.data = null;
    }

    /*Enum返回*/
    public ResponseVo(ResponseEnum responseEnum, T data) {
        this.code = responseEnum.getCode();
        this.status = responseEnum.isStatus();
        this.message = responseEnum.getMessage();
        this.data = data;
    }

    /*Enum返回---data Null*/
    public ResponseVo(ResponseEnum responseEnum) {
        this.code = responseEnum.getCode();
        this.status = responseEnum.isStatus();
        this.message = responseEnum.getMessage();
        this.data = null;
    }

    /*统一成功*/
    public static ResponseVo<Void> success() {
        return new ResponseVo<>(ResponseEnum.SUCCESS, null);
    }

    /*自定义data成功*/
    public static <T> ResponseVo<T> success(T data) {
        return new ResponseVo<>(ResponseEnum.SUCCESS, data);
    }

    /*全自定义成功*/
    public static <T> ResponseVo<T> success(ResponseEnum responseEnum, T data) {
        return new ResponseVo<>(responseEnum, data);
    }

    /*统一失败*/
    public static ResponseVo<Void> error() {
        return new ResponseVo<>(ResponseEnum.FAIL, null);
    }

    /*自定义data失败*/
    public static <T> ResponseVo<T> error(T data) {
        return new ResponseVo<>(ResponseEnum.FAIL, data);
    }

    /*全自定义失败*/
    public static <T> ResponseVo<T> error(ResponseEnum responseEnum, T data) {
        return new ResponseVo<>(responseEnum, data);
    }

}
