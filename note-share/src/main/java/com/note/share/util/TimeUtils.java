package com.note.share.util;

import cn.hutool.core.date.DateTime;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @Package：com.note.share.util
 * @Name：TimeUtil
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2024-02-03-19:35
 * @Description：时间工具类
 */
public class TimeUtils {
    /**
     * 判断是否是过去某几天
     *
     * @param dateTime
     * @return
     */
    public static boolean isNDays(DateTime dateTime, long days) {
        LocalDate localDate = LocalDate.now();
        LocalDate nDayDate = localDate.plusDays(days);
        LocalDate endDate = dateTime.toLocalDateTime().toLocalDate();
        return endDate.isBefore(nDayDate) && endDate.isAfter(localDate);
    }

    /**
     * 获取将来日期间的天数
     *
     * @param dateTime
     * @return
     */
    public static long getDateDay(DateTime dateTime) {
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime end = dateTime.toLocalDateTime();
        Duration duration = Duration.between(now, end);
        return Math.max(duration.toDays(), 1L);
    }
}
