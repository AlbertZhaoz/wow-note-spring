package com.note.share;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import com.note.config.constants.annotate.EnableGatewayFilter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

/**
 * @Package：com.note.share
 * @Name：NoteShareApplication
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2023-12-18-11:22
 * @Description：Spring启动
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableGatewayFilter/*直接访问拦截*/
public class NoteShareApplication {

    public static void main(String[] args) {
        SpringApplication.run(NoteShareApplication.class, args);
    }

    /*开启负载均衡*/
    @Bean
    @LoadBalanced
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    /*负载均衡策略*/
    @Bean
    public IRule randomRule() {
        return new RandomRule();
    }

}
