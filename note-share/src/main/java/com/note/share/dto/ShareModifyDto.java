package com.note.share.dto;

import com.note.share.service.ShareService;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Package：com.note.share.dto
 * @Name：ShareModifyDto
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2024-02-02-20:38
 * @Description：笔记分享修改状态
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ShareModifyDto {
    @NotBlank(groups = {ShareService.class}, message = "分享ID不能为空  ")
    @Schema(name = "shareId", description = "分享ID")
    private String shareId;
    @NotNull(groups = {ShareService.class}, message = "分享状态不能为空")
    @Min(groups = {ShareService.class}, value = 0L, message = "取值为0或1")
    @Max(groups = {ShareService.class}, value = 1L, message = "取值为0或1")
    @Schema(name = "type", description = "分享状态")
    private Integer type;
}
