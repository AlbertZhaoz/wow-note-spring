package com.note.share.dto;

import cn.hutool.core.date.DateTime;
import com.note.share.service.ShareService;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Future;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Package：com.note.share.dto
 * @Name：ShareDto
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2024-02-02-20:21
 * @Description：笔记分享Request参数模型
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ShareDto {
    @NotNull(groups = {ShareService.class}, message = "截止时间不能为空")
    @Future(groups = {ShareService.class}, message = "截止时间有误")
    @Schema(name = "endTime", description = "分享结束时间")
    private DateTime endTime;
    @NotBlank(groups = {ShareService.class}, message = "文件及ID不能为空")
    @Schema(name = "folderId", description = "文件夹ID ：否则=0")
    private String folderId;
    @NotBlank(groups = {ShareService.class}, message = "笔记ID不能为空")
    @Schema(name = "noteId", description = "笔记ID ：否则=0")
    private String noteId;
    @NotBlank(groups = {ShareService.class}, message = "用户名不能为空")
    @Schema(name = "username", description = "用户名")
    private String username;
}
