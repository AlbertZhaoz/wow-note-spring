package com.note.share.listeners;

import com.note.share.service.ShareService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.listener.KeyExpirationEventMessageListener;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;

/**
 * @Package：com.note.share.listeners
 * @Name：KeyExpiredListener
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2024-02-03-20:46
 * @Description：Redis Key过期监听
 */
@Slf4j
public class KeyExpiredListener extends KeyExpirationEventMessageListener {
    @Autowired
    private ShareService shareService;

    public KeyExpiredListener(RedisMessageListenerContainer listenerContainer) {
        super(listenerContainer);
    }

    /*获取消息*/
    @Override
    public void onMessage(Message message, byte[] pattern) {
        log.error("expired key:{}", message.toString());
        /*移除对应的ID*/
        shareService.removeShare(message.toString());
    }
}
