package com.note.notes.service;

import com.note.notes.domain.Folder;
import com.note.notes.dto.FolderDto;

import java.util.List;

/**
 * @Package：com.note.notes.service.impl
 * @Name：FolderService
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2023-12-20-15:02
 * @Description：文件夹业务接口
 */
public interface FolderService {
    /**
     * 创建目录文件夹
     *
     * @param folderDto Folder请求模型
     * @return
     */
    void createFolder(FolderDto folderDto);

    /**
     * 获取文件夹目录列表
     *
     * @param username 用户名
     * @param parentId 父级目录ID
     * @return List
     */
    List<Folder> listFolders(String username, String parentId);

    /**
     * 删除文件夹目录
     *
     * @param folderId 文件夹ID
     * @return boolean
     */
    boolean deleteFolder(String folderId);


    /**
     * 查询一个目录是否存在
     *
     * @param folderId
     * @return Folder
     */
    boolean isFolderExist(String folderId);
}
