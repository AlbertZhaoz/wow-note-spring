package com.note.notes.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.lang.ObjectId;
import com.note.notes.constants.ResponseCodeConstants;
import com.note.notes.dao.NoteRepository;
import com.note.notes.domain.Note;
import com.note.notes.dto.NoteDto;
import com.note.notes.pojo.NoteUpdatePojo;
import com.note.notes.service.FolderService;
import com.note.notes.service.NoteService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Package：com.note.notes.service.impl
 * @Name：NoteServiceImpl
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2023-12-21-12:09
 * @Description：笔记业务接口实现层
 */
@Slf4j
@Service
public class NoteServiceImpl implements NoteService {

    @Autowired
    private NoteRepository noteRepository;
    @Autowired
    private FolderService folderService;

    @Override
    public int recordNote(NoteDto noteDto) {
        /*无需判断用户是否存在 -> 已在网关验证*/
        /*判断目录ID是否存在*/
        if (!"root".equals(noteDto.getFolderId())) {
            if (!folderService.isFolderExist(noteDto.getFolderId())) {
                //目录不存在
                return ResponseCodeConstants.DONT_EXISTS_FOLDER;
            }
        }
        Note note = new Note();
        /*赋值*/
        note.setId(ObjectId.next());
        note.setTitle(noteDto.getTitle());
        note.setContent(noteDto.getContent());
        note.setTagId(noteDto.getTagId());
        note.setFolderId(noteDto.getFolderId());
        note.setUserId(noteDto.getUsername());
        note.setCreateTime(DateUtil.date());
        note.setUpdateTime(DateUtil.date());
        noteRepository.insertNote(note);
        return ResponseCodeConstants.NOTE_RECORD_SUCCESS;
    }

    @Override
    public int removeNote(String noteId, String username) {
        /*判断笔记是否存在*/
        if (!isNoteExits(noteId)) {
            //笔记不存在
            return ResponseCodeConstants.DONT_EXISTS_NOTE;
        }
        noteRepository.deleteNote(noteId, username);
        return ResponseCodeConstants.REMOVE_NOTE_SUCCESS;
    }

    @Override
    public int updateNote(NoteUpdatePojo noteUpdatePojo) {
        if (!"root".equals(noteUpdatePojo.getFolderId())) {
            /*判断目录ID是否存在*/
            if (!folderService.isFolderExist(noteUpdatePojo.getFolderId())) {
                //目录不存在
                return ResponseCodeConstants.DONT_EXISTS_FOLDER;
            }
        }
        /*判断笔记是否存在*/
        if (!isNoteExits(noteUpdatePojo.getNoteId())) {
            //笔记不存在
            return ResponseCodeConstants.DONT_EXISTS_NOTE;
        }
        noteRepository.updateNote(noteUpdatePojo);
        return ResponseCodeConstants.UPDATE_NOTE_SUCCESS;
    }

    @Override
    public List<Note> listNotes(String folderId, String username) {
        List<Note> noteList = noteRepository.selectNotes(folderId, username);
        if (CollUtil.isEmpty(noteList)) {
            try {
                throw new Exception("笔记不存在");
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        //设置分享状态
        noteList.forEach(note -> {
            note.setType(noteRepository.isSharedNote(note.getId()) ? 1 : 0);
            note.setShareId(noteRepository.getSharedId(note.getId()));
        });
        /* 相等于 */
        /*for (Note note : noteList) {
            if (noteRepository.isSharedNote(note.getId())) {
                note.setType(1);
            } else {
                note.setType(0);
            }
            note.setShareId(noteRepository.getSharedId(note.getId()));
        }*/

        return noteList;
    }

    @Override
    public Note viewNote(String noteId, String username) {
        if (!isNoteExits(noteId)) {
            throw new RuntimeException("笔记不存在");
        }
        return noteRepository.selectNote(noteId, username);
    }

    @Override
    public boolean isNoteExits(String noteId) {
        Note note = noteRepository.selectNote(noteId);
        return note != null;
    }

    @Override
    public Note viewShareNote(String noteId) {
        if (!isNoteExits(noteId)) {
            throw new RuntimeException("分享的笔记不存在");
        }
        return noteRepository.selectNote(noteId);
    }
}
