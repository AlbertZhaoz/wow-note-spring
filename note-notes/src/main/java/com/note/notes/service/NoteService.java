package com.note.notes.service;

import com.note.notes.domain.Note;
import com.note.notes.dto.NoteDto;
import com.note.notes.pojo.NoteUpdatePojo;

import java.util.List;

/**
 * @Package：com.note.notes.service
 * @Name：NoteService
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2023-12-21-12:09
 * @Description：笔记业务接口
 */
public interface NoteService {

    /**
     * 创建笔记
     *
     * @param noteDto
     * @return int
     */
    int recordNote(NoteDto noteDto);

    /**
     * 删除笔记
     *
     * @param noteId
     * @param username
     * @return int
     */
    int removeNote(String noteId, String username);

    /**
     * 更新笔记
     *
     * @param noteUpdatePojo
     * @return int
     */
    int updateNote(NoteUpdatePojo noteUpdatePojo);

    /**
     * 获取某目录下的笔记
     *
     * @param folderId
     * @param username
     * @return NoteList
     */
    List<Note> listNotes(String folderId, String username);

    /**
     * 查看某个笔记
     *
     * @param noteId
     * @param username
     * @return Note
     */
    Note viewNote(String noteId, String username);

    /**
     * 查看分享笔记
     *
     * @param noteId
     * @return Note
     */
    Note viewShareNote(String noteId);

    /**
     * 判断笔记是否存在
     *
     * @param noteId
     * @return
     */
    boolean isNoteExits(String noteId);
}
