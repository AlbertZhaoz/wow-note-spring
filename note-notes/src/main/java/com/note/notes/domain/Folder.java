package com.note.notes.domain;


import cn.hutool.core.date.DateTime;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.MongoId;


/**
 * @Package：com.note.notes.domain
 * @Name：Folder
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2023-12-20-15:01
 * @Description：文件夹文档数据模型
 */
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "folder") /*注意是：collection 不是 collation*/
public class Folder {
    @Id
    @MongoId
    @Field(name = "_id")
    @Schema(description = "文档ID")
    private String id;
    @Schema(description = "文件夹目录名")
    private String title;
    @Schema(description = "文件夹简介或介绍")
    private String content;
    @Field(name = "create_time")
    @Schema(description = "创建时间")
    private DateTime createTime;
    @Field(name = "parent_id")
    @Schema(description = "父目录ID null 则根目录")
    private String parenId;
    @JsonIgnore
    @Field(name = "user_id")
    @Schema(description = "用户名")
    private String userId;
}
