package com.note.notes.dto;

import com.note.notes.service.FolderService;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Package：com.note.notes.dto
 * @Name：FolderDto
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2023-12-22-18:33
 * @Description：文件夹Request参数模型
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FolderDto {
    @NotBlank(groups = {FolderService.class}, message = "用户名不能为空")
    @Schema(name = "username", description = "用户名")
    private String username;
    @NotBlank(groups = {FolderService.class}, message = "目录名不能为空")
    @Schema(name = "folderTitle", description = "文件夹标题")
    private String folderTitle;
    @Schema(name = "folderContent", description = "文件夹简介")
    private String folderContent;
    @NotBlank(groups = {FolderService.class}, message = "目录ID不能为空")
    @Schema(name = "folderId", description = "父级目录ID")
    private String parentId;
}
