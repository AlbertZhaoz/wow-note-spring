package com.note.notes.dto;

import com.note.notes.service.NoteService;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Package：com.note.notes.dto
 * @Name：NoteDto
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2023-12-23-23:15
 * @Description：笔记Request参数模型
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class NoteDto {
    @NotBlank(groups = {NoteService.class}, message = "用户名不能为空")
    @Schema(name = "username", description = "用户名")
    private String username;
    @NotBlank(groups = {NoteService.class}, message = "笔记标题不能为空")
    @Schema(name = "title", description = "笔记标题")
    private String title;
    @NotBlank(groups = {NoteService.class}, message = "笔记内容不能为空")
    @Schema(name = "content", description = "笔记内容")
    private String content;
    @NotBlank(groups = {NoteService.class}, message = "目录ID不能为空")
    @Schema(name = "folderId", description = "目录ID")
    private String folderId;
    @NotBlank(groups = {NoteService.class}, message = "标签ID不能为空")
    @Schema(name = "tagId", description = "标签ID")
    private String tagId;
}
