package com.note.notes.constants;

/**
 * @Package：com.note.notes.constants
 * @Name：ResponseCodeConstants
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2023-12-22-14:12
 * @Description：笔记操作状态码定义
 */
public class ResponseCodeConstants {
    /*系统级错误*/
    public static final int FAIL = -1;
    /*参数校验*/
    public static final int BAD_REQUEST = 400;
    /*Folder目录相关*/
    public static final int FOLDER_CREATE_FAIL = 500;
    public static final int FOLDER_EXISTS = 409;
    public static final int FOLDER_CREATE_SUCCESS = 209;

    public static final int FOLDER_NULL = 408;

    public static final int DELETE_FOLDER_FAIL = 407;
    public static final int DELETE_FOLDER_SUCCESS = 207;
    public static final int DONT_EXISTS_FOLDER = 406;

    /*Note笔记相关*/
    public static final int NOTE_RECORD_FAIL = 501;
    public static final int NOTE_UPDATE_FAIL = 502;
    public static final int NOTE_RECORD_SUCCESS = 200;


    public static final int DONT_EXISTS_NOTE = 410;
    public static final int REMOVE_NOTE_SUCCESS = 210;
    public static final int UPDATE_NOTE_SUCCESS = 211;
}
