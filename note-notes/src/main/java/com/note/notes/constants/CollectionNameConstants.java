package com.note.notes.constants;

/**
 * @Package：com.note.notes.constants
 * @Name：CollectionNameConstants
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2023-12-23-9:15
 * @Description：集合名定义类
 */
public class CollectionNameConstants {
    public static final String FOLDER = "folder";
    public static final String NOTE = "note";
    public static final String COMMENT = "comment";
    public static final String SHARE = "share";

}
