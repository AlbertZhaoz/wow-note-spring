package com.note.notes.constants;

/**
 * @Package：com.note.notes.constants
 * @Name：ResponseMessageConstants
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2023-12-22-14:13
 * @Description：笔记操作消息定义
 */
public class ResponseMessageConstants {
    /*Folder目录相关*/
    public static final String FOLDER_CREATE_FAIL = "文件夹创建失败";
    public static final String FOLDER_EXISTS = "发现同名文件夹";
    public static final String FOLDER_CREATE_SUCCESS = "文件夹创建成功";

    public static final String FOLDER_NULL = "你的笔记为空";

    public static final String DELETE_FOLDER_FAIL = "删除文件夹失败";
    public static final String DELETE_FOLDER_SUCCESS = "删除文件夹成功";
    public static final String DONT_EXISTS_FOLDER = "文件夹不存在";

    /*Note笔记相关*/
    public static final String NOTE_RECORD_FAIL = "记录笔记失败";
    public static final String NOTE_UPDATE_FAIL = "更新笔记失败";
    public static final String NOTE_RECORD_SUCCESS = "记录笔记成功";

    public static final String DONT_EXISTS_NOTE = "笔记不存在";
    public static final String REMOVE_NOTE_SUCCESS = "笔记删除成功";
    public static final String UPDATE_NOTE_SUCCESS = "更新笔记成功";

}
