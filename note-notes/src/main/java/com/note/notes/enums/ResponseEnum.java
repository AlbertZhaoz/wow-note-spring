package com.note.notes.enums;

import com.note.notes.constants.ResponseCodeConstants;
import com.note.notes.constants.ResponseMessageConstants;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Getter;

/**
 * @Package：com.note.notes.enums
 * @Name：ResponseEnum
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2023-12-20-15:22
 * @Description：统一返回枚举类
 */
@Getter
@Tag(name = "ResponseEnum", description = "笔记模块统一返回枚举类")
public enum ResponseEnum {
    /*默认状态*/
    SUCCESS(200, true, "请求成功"),
    FAIL(400, false, "请求失败"),

    /*文件夹操作状态*/
    FOLDER_CREATE_FAIL(ResponseCodeConstants.FOLDER_CREATE_FAIL, false, ResponseMessageConstants.FOLDER_CREATE_FAIL),
    FOLDER_EXISTS(ResponseCodeConstants.FOLDER_EXISTS, false, ResponseMessageConstants.FOLDER_EXISTS),
    FOLDER_CREATE_SUCCESS(ResponseCodeConstants.FOLDER_CREATE_SUCCESS, true, ResponseMessageConstants.FOLDER_CREATE_SUCCESS),
    FOLDER_NULL(ResponseCodeConstants.FOLDER_NULL, false, ResponseMessageConstants.FOLDER_NULL),
    DELETE_FOLDER_FAIL(ResponseCodeConstants.DELETE_FOLDER_FAIL, false, ResponseMessageConstants.DELETE_FOLDER_FAIL),
    DELETE_FOLDER_SUCCESS(ResponseCodeConstants.DELETE_FOLDER_SUCCESS, false, ResponseMessageConstants.DELETE_FOLDER_SUCCESS),
    DONT_EXISTS_FOLDER(ResponseCodeConstants.DONT_EXISTS_FOLDER, false, ResponseMessageConstants.DONT_EXISTS_FOLDER),

    /*笔记操作状态*/
    NOTE_REPORT_FAIL(ResponseCodeConstants.NOTE_RECORD_FAIL, false, ResponseMessageConstants.NOTE_RECORD_FAIL),
    NOTE_UPDATE_FAIL(ResponseCodeConstants.NOTE_UPDATE_FAIL, false, ResponseMessageConstants.NOTE_UPDATE_FAIL),
    NOTE_REPORT_SUCCESS(ResponseCodeConstants.NOTE_RECORD_SUCCESS, true, ResponseMessageConstants.NOTE_RECORD_SUCCESS),
    DONT_EXITS_NOTE(ResponseCodeConstants.DONT_EXISTS_NOTE, false, ResponseMessageConstants.DONT_EXISTS_NOTE),
    REMOVE_NOTE_SUCCESS(ResponseCodeConstants.REMOVE_NOTE_SUCCESS, true, ResponseMessageConstants.REMOVE_NOTE_SUCCESS),
    UPDATE_NOTE_SUCCESS(ResponseCodeConstants.UPDATE_NOTE_SUCCESS, true, ResponseMessageConstants.UPDATE_NOTE_SUCCESS);


    /*状态码*/
    private final int code;
    /*请求状态*/
    private final boolean status;
    /*请求结果*/
    private final String message;

    /*构造*/
    ResponseEnum(int code, boolean status, String message) {
        this.code = code;
        this.status = status;
        this.message = message;
    }


}
