package com.note.notes.pojo;

import com.note.notes.service.NoteService;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.MongoId;

/**
 * @Package：com.note.notes.pojo
 * @Name：NoteUpdatePojo
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2023-12-24-22:48
 * @Description：笔记更新数据模型
 */
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "note")
public class NoteUpdatePojo {
    @Id
    @MongoId
    @Field(name = "id")
    @Schema(description = "笔记ID")
    @NotBlank(groups = {NoteService.class}, message = "笔记ID不能为空")
    private String noteId;
    @Schema(description = "笔记短标题")
    @NotBlank(groups = {NoteService.class}, message = "把笔记标题不能为空")
    private String title;
    @Schema(description = "笔记内容 MD格式")
    @NotBlank(groups = {NoteService.class}, message = "笔记内容不能为空")
    private String content;
    @Field(name = "folder_id")
    @Schema(description = "目录ID 根笔记则null")
    @NotBlank(groups = {NoteService.class}, message = "目录ID不能为空")
    private String folderId;
    @Field(name = "tag_id")
    @Schema(description = "标签ID")
    @NotBlank(groups = {NoteService.class}, message = "标签ID不能为空")
    private String tagId;
    @Field(name = "user_id")
    @Schema(description = "用户名")
    @NotBlank(groups = {NoteService.class}, message = "用户名不能为空")
    private String username;
}
