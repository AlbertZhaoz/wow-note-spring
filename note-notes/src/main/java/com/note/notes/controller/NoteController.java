package com.note.notes.controller;

import com.note.notes.constants.ResponseCodeConstants;
import com.note.notes.dto.NoteDto;
import com.note.notes.enums.ResponseEnum;
import com.note.notes.pojo.NoteUpdatePojo;
import com.note.notes.service.NoteService;
import com.note.notes.vo.ResponseVo;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.constraints.NotBlank;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @Package：com.note.notes.controller
 * @Name：NoteController
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2023-12-20-14:52
 * @Description：笔记本体控制类
 */
@Slf4j
@Validated
@RestController
@RequestMapping("/note")
@Tag(name = "NoteController", description = "笔记操作相关接口")
public class NoteController {

    @Autowired
    private NoteService noteService;

    /*记录笔记|获取笔记列表|获取笔记内容|更新笔记|删除笔记*/

    @ResponseBody
    @PostMapping("/recordNote")
    @Operation(summary = "recordNote", description = "记录新笔记")
    public ResponseVo recordNote(@RequestBody @Validated({NoteService.class}) NoteDto noteDto) {
        /*创建笔记*/
        int recordStatus = noteService.recordNote(noteDto);
        return switch (recordStatus) {
            case ResponseCodeConstants.DONT_EXISTS_FOLDER -> new ResponseVo(ResponseEnum.DONT_EXISTS_FOLDER);
            case ResponseCodeConstants.NOTE_RECORD_SUCCESS -> new ResponseVo(ResponseEnum.NOTE_REPORT_SUCCESS);
            default -> new ResponseVo(ResponseEnum.NOTE_REPORT_FAIL);
        };
    }

    @ResponseBody
    @GetMapping("/listNotes")
    @Operation(summary = "listNotes", description = "获取笔记列表")
    public ResponseVo listNotes(@RequestParam @NotBlank(message = "目录ID不能为空") String folderId, @RequestParam @NotBlank(message = "用户名不能为空") String username) {
        return new ResponseVo<>(ResponseEnum.SUCCESS, noteService.listNotes(folderId, username));
    }

    @ResponseBody
    @GetMapping("/viewNote")
    @Operation(summary = "viewNote", description = "查看笔记")
    public ResponseVo viewNote(@RequestParam @NotBlank(message = "笔记ID不能为空") String noteId, @RequestParam @NotBlank(message = "用户名不能为空") String username) {
        return new ResponseVo<>(ResponseEnum.SUCCESS, noteService.viewNote(noteId, username));
    }

    @ResponseBody
    @PutMapping("/updateNote")
    @Operation(summary = "updateNote", description = "更新笔记内容")
    public ResponseVo updateNote(@RequestBody @Validated({NoteService.class}) NoteUpdatePojo noteUpdatePojo) {
        /*更新笔记*/
        int updateStatus = noteService.updateNote(noteUpdatePojo);
        return switch (updateStatus) {
            case ResponseCodeConstants.DONT_EXISTS_NOTE -> new ResponseVo(ResponseEnum.DONT_EXITS_NOTE);
            case ResponseCodeConstants.DONT_EXISTS_FOLDER -> new ResponseVo(ResponseEnum.DONT_EXISTS_FOLDER);
            case ResponseCodeConstants.UPDATE_NOTE_SUCCESS -> new ResponseVo(ResponseEnum.UPDATE_NOTE_SUCCESS);
            default -> new ResponseVo(ResponseEnum.FAIL);
        };
    }

    @ResponseBody
    @DeleteMapping("/removeNote")
    @Operation(summary = "removeNote", description = "删除笔记")
    public ResponseVo removeNote(@RequestParam @NotBlank(message = "笔记ID不能为空") String noteId, @RequestParam @NotBlank(message = "用户名不能为空") String username) {
        /*删除笔记*/
        int removeStatus = noteService.removeNote(noteId, username);
        return switch (removeStatus) {
            case ResponseCodeConstants.DONT_EXISTS_NOTE -> new ResponseVo(ResponseEnum.DONT_EXITS_NOTE);
            case ResponseCodeConstants.REMOVE_NOTE_SUCCESS -> new ResponseVo(ResponseEnum.REMOVE_NOTE_SUCCESS);
            default -> new ResponseVo(ResponseEnum.FAIL);
        };
    }

    @ResponseBody
    @GetMapping("/viewShareNote/{noteId}")
    @Operation(summary = "viewShareNote", description = "查看分享的笔记")
    public ResponseVo viewShareNote(@PathVariable("noteId") @NotBlank(message = "笔记ID不能为空") String noteId) {
        return new ResponseVo<>(ResponseEnum.SUCCESS, noteService.viewShareNote(noteId));
    }
}
