package com.note.notes.controller;

import com.note.notes.constants.ResponseCodeConstants;
import com.note.notes.domain.Folder;
import com.note.notes.dto.FolderDto;
import com.note.notes.enums.ResponseEnum;
import com.note.notes.service.FolderService;
import com.note.notes.vo.ResponseVo;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.constraints.NotBlank;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Package：com.note.notes.controller
 * @Name：FolderController
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2023-12-18-18:55
 * @Description：笔记文件夹目录控制类
 */

@Slf4j
@Validated
@RestController
@RequestMapping("/folder")
@Tag(name = "NoteFolderController", description = "笔记文件夹目操作相关接口")
public class FolderController {

    @Autowired
    private FolderService folderService;

    /*文件夹创建|文件夹列表|文件夹删除*/

    @ResponseBody
    @PostMapping("/createFolder")
    @Operation(summary = "createFolder", description = "创建文件夹")
    public ResponseVo createFolder(@RequestBody @Validated({FolderService.class}) FolderDto folderDto) {
        folderService.createFolder(folderDto);
        return new ResponseVo(ResponseEnum.FOLDER_CREATE_SUCCESS);
    }

    @ResponseBody
    @GetMapping("/listFolders")
    @Operation(summary = "listFolders", description = "获取文件夹列表")
    public ResponseVo listFolders(@RequestParam @NotBlank(message = "用户名不能为空") String username, @RequestParam @NotBlank(message = "父级目录ID不能为空") String parentId) {
        List<Folder> folders = folderService.listFolders(username, parentId);
        if (CollectionUtils.isEmpty(folders)) {
            return new ResponseVo(ResponseEnum.FOLDER_NULL, null);
        }
        return new ResponseVo(ResponseEnum.SUCCESS, folders);
    }

    @ResponseBody
    @DeleteMapping("/deleteFolder")
    @Operation(summary = "deleteFolder", description = "删除笔记主文件夹")
    public ResponseVo deleteFolder() {
        return new ResponseVo(ResponseCodeConstants.FAIL, false, "加班开发中...");
    }
}
