package com.note.notes.dao;

import com.note.notes.domain.Folder;

import java.util.List;

/**
 * @Package：com.note.notes.dao
 * @Name：FolderRepository
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2023-12-20-15:04
 * @Description：文件夹业务数据库操作接口
 */
public interface FolderRepository {
    /**
     * 创建目录文件夹
     *
     * @param folder Folder请求模型
     * @return boolean
     */
    void createFolder(Folder folder);

    /**
     * 获取文件夹目录列表
     *
     * @param username 用户名
     * @param parentId
     * @return List
     */
    List<Folder> selectFolders(String username, String parentId);

    /**
     * 删除文件夹目录
     *
     * @param folderId 文件夹ID
     * @return boolean
     */
    void deleteFolder(String folderId);


    /**
     * 查询一个目录是否存在
     *
     * @param folderId
     * @return Folder
     */
    Folder selectFolder(String folderId);
}
