package com.note.notes.dao.impl;

import com.note.notes.constants.CollectionNameConstants;
import com.note.notes.dao.FolderRepository;
import com.note.notes.domain.Folder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Package：com.note.notes.dao.impl
 * @Name：FolderRepositoryImpl
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2023-12-20-15:06
 * @Description：文件夹业务数据库操作接口实现
 */
@Repository
public class FolderRepositoryImpl implements FolderRepository {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public void createFolder(Folder folder) {
        /*执行失败则报错*/
        mongoTemplate.insert(folder, CollectionNameConstants.FOLDER);
    }

    @Override
    public List<Folder> selectFolders(String username, String parentId) {
        //条件
        Query query = new Query(Criteria.where("user_id").is(username).and("parent_id").is(parentId));
        //Find查询
        return mongoTemplate.find(query, Folder.class);
    }

    @Override
    public void deleteFolder(String folderId) {
        /*暂时不写删除业务*/
    }

    @Override
    public Folder selectFolder(String folderId) {
        Query query = new Query(Criteria.where("id").is(folderId));
        return mongoTemplate.findOne(query, Folder.class);
    }
}
