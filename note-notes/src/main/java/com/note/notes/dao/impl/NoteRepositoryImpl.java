package com.note.notes.dao.impl;

import cn.hutool.core.date.DateUtil;
import com.note.config.constants.domain.Share;
import com.note.notes.constants.CollectionNameConstants;
import com.note.notes.dao.NoteRepository;
import com.note.notes.domain.Note;
import com.note.notes.pojo.NoteUpdatePojo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Package：com.note.notes.dao.impl
 * @Name：NoreRepositoryImpl
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2023-12-21-12:07
 * @Description：笔记业务数据库操作实现
 */
@Slf4j
@Repository
public class NoteRepositoryImpl implements NoteRepository {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public void insertNote(Note note) {
        /*存入文档*/
        mongoTemplate.insert(note, CollectionNameConstants.NOTE);
    }

    @Override
    public void deleteNote(String noteId, String username) {
        /*条件*/
        Query query = new Query(Criteria.where("id").is(noteId).and("user_id").is(username));
        mongoTemplate.remove(query, Note.class);
    }

    @Override
    public void updateNote(NoteUpdatePojo noteUpdatePojo) {
        /*更新条件*/
        Query query = new Query(
                Criteria.where("id").is(noteUpdatePojo.getNoteId())
                        .and("user_id").is(noteUpdatePojo.getUsername())
                        .and("folder_id").is(noteUpdatePojo.getFolderId()));
        /*更新数据*/
        Update update = new Update();
        update.set("title", noteUpdatePojo.getTitle());
        update.set("content", noteUpdatePojo.getContent());
        update.set("folder_id", noteUpdatePojo.getFolderId());
        update.set("tag_id", noteUpdatePojo.getTagId());
        update.set("update_time", DateUtil.date());
        /*执行更新（第一个）*/
        mongoTemplate.updateFirst(query, update, Note.class);
    }

    @Override
    public List<Note> selectNotes(String folderId, String username) {
        Query query = new Query(Criteria.where("folder_id").is(folderId).and("user_id").is(username));
        return mongoTemplate.find(query, Note.class);
    }

    @Override
    public Note selectNote(String noteId, String username) {
        Query query = new Query(Criteria.where("id").is(noteId).and("user_id").is(username));
        return mongoTemplate.findOne(query, Note.class);
    }

    @Override
    public Note selectNote(String noteId) {
        Query query = new Query(Criteria.where("id").is(noteId));
        return mongoTemplate.findOne(query, Note.class);
    }

    @Override
    public boolean isSharedNote(String noteId) {
        Query query = new Query(Criteria.where("note_id").is(noteId).and("type").is(1));
        return mongoTemplate.exists(query, CollectionNameConstants.SHARE);
    }

    @Override
    public String getSharedId(String noteId) {
        Query query = new Query(Criteria.where("note_id").is(noteId));
        Share share = mongoTemplate.findOne(query, Share.class, CollectionNameConstants.SHARE);
        if (share != null) {
            return share.getId();
        } else {
            /*throw new RuntimeException("该分享不存在,请重试");*/
            return "0";
        }
    }
}
