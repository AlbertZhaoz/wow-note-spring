package com.note.notes.dao;

import com.note.notes.domain.Note;
import com.note.notes.pojo.NoteUpdatePojo;

import java.util.List;

/**
 * @Package：com.note.notes.dao
 * @Name：NoteRepository
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2023-12-21-12:06
 * @Description：笔记业务数据库操作接口
 */
public interface NoteRepository {

    /**
     * 创建笔记
     *
     * @param note
     * @return int
     */
    void insertNote(Note note);

    /**
     * 删除笔记
     *
     * @param noteId
     * @param username
     * @return int
     */
    void deleteNote(String noteId, String username);

    /**
     * 更新笔记
     *
     * @param noteUpdatePojo
     * @return int
     */
    void updateNote(NoteUpdatePojo noteUpdatePojo);

    /**
     * 获取某目录下的笔记
     *
     * @param folderId
     * @param username
     * @return NoteList
     */
    List<Note> selectNotes(String folderId, String username);

    /**
     * 查看某个笔记
     *
     * @param noteId
     * @param username
     * @return Note
     */
    Note selectNote(String noteId, String username);


    /**
     * 查询一个文档是否存在·
     *
     * @param noteId
     * @return Note
     */
    Note selectNote(String noteId);

    /**
     * 判断是否分享的笔记
     *
     * @param noteId
     * @return
     */
    boolean isSharedNote(String noteId);

    /**
     * 获取分享ID
     *
     * @param noteId
     * @return
     */
    String getSharedId(String noteId);
}
