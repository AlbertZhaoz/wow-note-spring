package com.note.gateway.config;

import com.note.gateway.util.ResponseVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.sql.SQLException;

/**
 * @Package：com.note.gateway
 * @Name：GlobalExceptionHandler
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2023-12-23-18:46
 * @Description：全局统一异常处理
 */
@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {
    /**
     * SQL异常处理类
     *
     * @param e
     * @return
     */
    @ExceptionHandler(SQLException.class)
    public ResponseVo SQLSyntaxErrorException(Exception e) {
        return new ResponseVo(-1, false, "用户不存在或密码错误");
    }
}
