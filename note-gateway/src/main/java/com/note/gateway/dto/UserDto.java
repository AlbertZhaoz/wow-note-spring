package com.note.gateway.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Package：com.note.user.dto
 * @Name：UserDto
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2023-12-11-15:32
 * @Description：用户登录请求参数Params模型
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDto implements Serializable {
    @Schema(name = "username", description = "用户名")
    @NotBlank(message = "用户名不能为空")
    private String username;
    @Schema(name = "password", description = "用户密码")
    @NotBlank(message = "用户密码不能为空")
    private String password;
    @Schema(name = "email", description = "邮箱")
    @NotBlank(message = "邮箱不能为空")
    @Email(message = "邮箱格式有误")
    private String email;
}
