package com.note.gateway.constants;

/**
 * @Package：com.note.user.constants
 * @Name：ResponseCodeConstants
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2023-12-12-9:38
 * @Description：内部服务间状态码定义
 */
public class ResponseCodeConstants {

    /*Exception异常*/
    public static final int ERROR = -1; // 出现异常
}
