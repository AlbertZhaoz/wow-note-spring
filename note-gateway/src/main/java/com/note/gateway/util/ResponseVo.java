package com.note.gateway.util;

import com.note.gateway.enums.ResponseEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Data;

import java.io.Serializable;

/**
 * @Package：com.note.user.util
 * @Name：ResponseVo
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2023-12-11-17:52
 * @Description：通用返回类
 */
@Data
@Tag(name = "ResponseVo", description = "统一返回处理")
public class ResponseVo<T> implements Serializable {
    @Schema(name = "code", description = "状态码")
    private int code;
    @Schema(name = "status", description = "请求状态")
    private boolean status;
    @Schema(name = "message", description = "状态说明")
    private String message;
    @Schema(name = "data", description = "状态数据")
    private T data;

    /*无参构造*/
    ResponseVo() {
        super();
    }

    /*code | status | message 构造*/
    public ResponseVo(int code, boolean status, String message) {
        this.code = code;
        this.status = status;
        this.message = message;
        this.data = null;
    }

    /*code | status | message | data 构造*/
    public ResponseVo(int code, boolean status, String message, T data) {
        this.code = code;
        this.status = status;
        this.message = message;
        this.data = data;
    }

    /*枚举构造*/
    public ResponseVo(ResponseEnum responseEnum, T data) {
        this.code = responseEnum.getCode();
        this.status = responseEnum.isStatus();
        this.message = responseEnum.getMessage();
        this.data = data;
    }

    /*枚举构造*/
    public ResponseVo(ResponseEnum responseEnum) {
        this.code = responseEnum.getCode();
        this.status = responseEnum.isStatus();
        this.message = responseEnum.getMessage();
        this.data = null;
    }

    /**
     * 返回成功业务状态|描述
     *
     * @return
     */
    public static ResponseVo<Void> success() {
        return new ResponseVo<Void>(ResponseEnum.SUCCESS, null);
    }

    /**
     * 返回成功业务状态|描述|自定义数据
     *
     * @param data
     * @param <T>
     * @return
     */
    public static <T> ResponseVo<T> success(T data) {
        return new ResponseVo<T>(ResponseEnum.SUCCESS, data);
    }

    /**
     * 返回成功业务自定义状态|描述|数据
     *
     * @param responseEnum
     * @param data
     * @param <T>
     * @return
     */
    public static <T> ResponseVo<T> success(ResponseEnum responseEnum, T data) {
        if (responseEnum == null) {
            return success(data);
        }
        return new ResponseVo<T>(responseEnum, data);
    }

    /**
     * 返回失败业务状态|描述
     *
     * @return
     */
    public static ResponseVo<Void> fail() {
        return new ResponseVo<Void>(ResponseEnum.FAIL, null);
    }

    /**
     * 返回失败业务状态|描述|自定义数据
     *
     * @param data
     * @param <T>
     * @return
     */
    public static <T> ResponseVo<T> fail(T data) {
        return new ResponseVo<T>(ResponseEnum.FAIL, data);
    }

    /**
     * 返回失败业务自定义状态|描述|数据
     *
     * @param responseEnum
     * @param data
     * @param <T>
     * @return
     */
    public static <T> ResponseVo<T> fail(ResponseEnum responseEnum, T data) {
        if (null == responseEnum) {
            return fail(data);
        }
        return new ResponseVo<T>(responseEnum, data);
    }

}
