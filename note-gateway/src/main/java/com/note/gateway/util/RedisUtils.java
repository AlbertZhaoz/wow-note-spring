package com.note.gateway.util;

import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * @Package：com.note.user.util
 * @Name：RedisUtils
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2023-12-18-15:01
 * @Description：Redis工具类
 */

@Component
public class RedisUtils {
    @Autowired
    private RedisTemplate redisTemplate;
    public static RedisUtils redisUtils = null;

    @PostConstruct /*执行初始化的方法，并且只会被 执行一次*/
    public void init() {
        redisUtils = this;
        redisUtils.redisTemplate = this.redisTemplate;
        // 设置值（value）的序列化采用StringRedisSerializer。 Jackson2JsonRedisSerializer时会带双引号
        redisTemplate.setValueSerializer(new StringRedisSerializer());
        redisTemplate.setHashValueSerializer(new StringRedisSerializer());
        // 设置键（key）的序列化采用StringRedisSerializer。
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        redisTemplate.setHashKeySerializer(new StringRedisSerializer());
    }

    /**
     * 存入数据
     *
     * @param key      键
     * @param value    值
     * @param time     周期
     * @param timeUnit 单位
     */
    public static void saveValue(String key, Object value, int time, TimeUnit timeUnit) {
        redisUtils.redisTemplate.opsForValue().set(key, value, (long) time, timeUnit);
    }

    /**
     * 取出数据
     *
     * @param key 键
     * @param <T> 返回类型
     * @return T
     */

    public static <T> T getValue(String key) {
        ValueOperations<String, T> valueOperations = redisUtils.redisTemplate.opsForValue();
        return valueOperations.get(key);
    }

    /**
     * 移除数据
     *
     * @param key 键
     * @return boolean
     */
    public static boolean removeValue(String key) {
        return redisUtils.redisTemplate.delete(key);
    }

    /**
     * 判断是否存在
     *
     * @param key
     * @return
     */
    public static boolean exists(String key) {
        return redisUtils.redisTemplate.hasKey(key);
    }

    /**
     * 判断是否已过去
     *
     * @param key
     * @return
     */
    public static boolean isExpire(String key) {
        return expire(key) <= 0L;
    }

    /**
     * 获取key过期事件
     *
     * @param key
     * @return
     */
    public static long expire(String key) {
        return redisUtils.redisTemplate.opsForValue().getOperations().getExpire(key);
    }
}
