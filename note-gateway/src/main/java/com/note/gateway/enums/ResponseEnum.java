package com.note.gateway.enums;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;

/**
 * @Package：com.note.user.enums
 * @Name：ResponseEnum
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2023-12-11-19:09
 * @Description：统一返回枚举类
 */
@Getter
@Schema(name = "ResponseEnum", description = "用户模块统一返回枚举类")
public enum ResponseEnum {
    /*默认状态定义*/
    SUCCESS(200, true, "请求成功"),
    FAIL(400, false, "请求失败");
    /*状态码（*）*/
    private final int code;
    /*请求状态（*）*/
    private final boolean status;
    /*状态描述（*）*/
    private final String message;

    ResponseEnum(int code, boolean status, String message) {
        this.code = code;
        this.status = status;
        this.message = message;
    }

}
