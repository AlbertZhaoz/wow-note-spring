package com.note.gateway.enums;

import lombok.Getter;

/**
 * @Package：com.note.gateway.enums
 * @Name：JwtErrorCode
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2023-12-18-22:39
 * @Description：Jwt验证错误信息枚举类
 */

@Getter
public enum JwtErrorCode {
    /*令牌异常*/
    ERROR_TOKEN_EMPTY("令牌不能为空"),
    ERROR_SIGNATURE_MISMATCH("签名不匹配"),
    ERROR_TOKEN_EXPIRED("令牌已过期"),
    ERROR_ALGORITHM_MISMATCH("算法不匹配"),
    ERROR_PAYLOAD_INVALID("参数已失效"),
    ERROR_INVALID_TOKEN("无效令牌"),
    ERROR_DONT_EXITS_TOKEN("令牌已更新或不存在"),

    /*账号状态*/
    USER_NOT_FOUND("用户名不存在"),
    WRONG_PASSWORD("用户密码错误"),
    TOKEN_USER_MISMATCH("数据不匹配");

    /*error message*/
    private final String message;

    JwtErrorCode(String message) {
        this.message = message;
    }

}
