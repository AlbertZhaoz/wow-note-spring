package com.note.gateway.service.impl;

import com.note.gateway.domain.User;
import com.note.gateway.dto.UserDto;
import com.note.gateway.service.UserService;
import com.note.gateway.service.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Package：com.note.gateway.service.impl
 * @Name：UserServiceImpl
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2023-12-23-19:15
 * @Description：用户验证接口实现
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public User userInfo(UserDto userDto) {
        return userMapper.userInfo(userDto);
    }
}
