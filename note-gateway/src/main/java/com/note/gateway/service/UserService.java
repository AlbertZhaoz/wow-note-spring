package com.note.gateway.service;


import com.note.gateway.domain.User;
import com.note.gateway.dto.UserDto;

/**
 * @Package：com.note.gateway.service
 * @Name：UserService
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2023-12-23-19:15
 * @Description：用户数据验证接口
 */
public interface UserService {
    /**
     * 获取用户信息
     *
     * @param userDto
     * @return
     */
    User userInfo(UserDto userDto);
}
