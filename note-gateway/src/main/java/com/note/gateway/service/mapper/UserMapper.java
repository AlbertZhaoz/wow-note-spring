package com.note.gateway.service.mapper;
import com.note.gateway.dto.UserDto;
import org.apache.ibatis.annotations.Mapper;
import com.note.gateway.domain.User;

/**
 * @Package：com.note.gateway.service
 * @Name：UserIpml
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2023-12-23-17:28
 * @Description：用户服务调用Feign接口
 */

/*@FeignClient(value = "note-user") 服务间调用*/
@Mapper
public interface UserMapper {

    /**
     * 获取用户信息
     *
     * @param userDto
     * @return
     */
    User userInfo(UserDto userDto);
}
