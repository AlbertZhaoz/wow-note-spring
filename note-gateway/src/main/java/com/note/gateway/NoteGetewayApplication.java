package com.note.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Package：com.note.geteway
 * @Name：NoteGetewayApplication
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2023-12-11-11:22
 * @Description：Spring启动
 */
@SpringBootApplication
/*@EnableFeignClients*/
public class NoteGetewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(NoteGetewayApplication.class, args);
    }

    /*不引入会报 Feign 异常 需要手动引入HttpMessageConverters*/
    /*@Bean
    @ConditionalOnMissingBean
    public HttpMessageConverters messageConverters(ObjectProvider<? extends HttpMessageConverter<?>> converters) {
        return new HttpMessageConverters(converters.orderedStream().collect(Collectors.toList()));
    }*/
}
