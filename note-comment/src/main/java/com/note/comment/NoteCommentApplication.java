package com.note.comment;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

/**
 * @Package：com.note.cooment
 * @Name：NoteCommentApplication
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2023-12-18-11:22
 * @Description：Spring启动
 */

@SpringBootApplication
@EnableDiscoveryClient
public class NoteCommentApplication {

    public static void main(String[] args) {
        SpringApplication.run(NoteCommentApplication.class, args);
    }

    /*开启负载均衡*/
    @Bean
    @LoadBalanced
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    /*负载均衡策略*/
    @Bean
    public IRule randomRule() {
        return new RandomRule();
    }

}
