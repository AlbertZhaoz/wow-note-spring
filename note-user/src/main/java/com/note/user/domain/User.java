package com.note.user.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.note.user.constants.TableNameConstants;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.relational.core.mapping.Column;

import java.sql.Date;

/**
 * @Package：com.note.user.domain
 * @Name：User
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2023-12-11-9:40
 * @Description：用户数据模型
 */
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = TableNameConstants.TABLE_USER)
@TableName(TableNameConstants.TABLE_USER)
@Schema(description = "用户数据模型")
public class User {
    @Id
    @TableId(value = "id", type = IdType.AUTO)
    @Schema(description = "自增主键ID")
    private int id;
    @Column
    @Schema(description = "用户名")
    private String username;
    @Column
    @Schema(description = "密码")
    private String password;
    @Column
    @Schema(description = "邮箱")
    private String email;
    @Column
    @Schema(description = "用户状态")
    private int status;
    @Column
    @TableField(fill = FieldFill.INSERT) /*当前时间*/
    @Schema(description = "注册时间")
    private Date createTime;
    @Column
    @Schema(description = "访问令牌")
    private String token;
}
