package com.note.user.service;

import com.note.user.dto.UserDto;

/**
 * @Package：com.note.user.service
 * @Name：RefisterService
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2023-12-11-11:22
 * @Description：注册服务接口定义
 */
public interface UserRegisterService {
    /**
     * 用户注册
     *
     * @param userDto 注册用户Params模型
     * @return int
     */
    int registerUser(UserDto userDto);
}
