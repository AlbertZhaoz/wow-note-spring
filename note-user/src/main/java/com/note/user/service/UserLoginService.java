package com.note.user.service;

import com.note.user.domain.User;
import com.note.user.dto.UserDto;

/**
 * @Package：com.note.user.service
 * @Name：LoginService
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2023-12-11-11:21
 * @Description：登录服务接口
 */
public interface UserLoginService {
    /**
     * 用户登录验证
     *
     * @param userDto 用户登录Params模型
     * @return int
     */
    int validateUser(UserDto userDto);

    /**
     * 获取用户信息
     *
     * @param userDto 登录数据模型
     * @return User
     */
    User getUserInfo(UserDto userDto);

    /**
     * 判断用户是否存在
     *
     * @param userDto
     * @return
     */
    boolean isUserExist(UserDto userDto);
}
