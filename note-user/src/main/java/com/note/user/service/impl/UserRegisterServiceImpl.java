package com.note.user.service.impl;

import cn.hutool.core.date.DateUtil;
import com.note.user.constants.ForumDefaultConstants;
import com.note.user.constants.ResponseCodeConstants;
import com.note.user.dao.UserRegisterRepository;
import com.note.user.dto.UserDto;
import com.note.user.pojo.UserRegisterPojo;
import com.note.user.service.UserLoginService;
import com.note.user.service.UserRegisterService;
import com.note.user.util.DigestUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Package：com.note.user.service.impl
 * @Name：RegisterServiceImpl
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2023-12-11-11:23
 * @Description：注册服务实现
 */
@Service
public class UserRegisterServiceImpl implements UserRegisterService {
    @Autowired
    private UserRegisterRepository userRegisterRepository;
    @Autowired
    private UserLoginService userLoginService;

    /*用户注册*/
    @Override
    public int registerUser(UserDto userDto) {
        //RegisterPojo
        UserRegisterPojo userRegisterPojo = new UserRegisterPojo();
        userRegisterPojo.setUsername(userDto.getUsername());
        //账号是否存在
        if (userLoginService.isUserExist(new UserDto(userRegisterPojo.getUsername(), userRegisterPojo.getPassword(), null))) {
            return ResponseCodeConstants.USERNAME_ALREADY_EXISTS;
        }
        userRegisterPojo.setPassword(DigestUtil.encryptMd5hex(userDto.getPassword()));
        userRegisterPojo.setEmail(userDto.getEmail());
        //状态默认：1
        userRegisterPojo.setState(ForumDefaultConstants.USER_REGISTER_STATE);
        //注册时间
        userRegisterPojo.setCreate_time(DateUtil.date());
        userRegisterPojo.setToken(null);
        int registerState = userRegisterRepository.saveUser(userRegisterPojo);
        if (registerState > 0) {
            return ResponseCodeConstants.REGISTER_SUCCESS;
        }
        return ResponseCodeConstants.REGISTER_FAIL;
    }
}
