package com.note.user.service.impl;

import com.note.user.constants.ResponseCodeConstants;
import com.note.user.dao.UserInfoRepository;
import com.note.user.dao.UserLoginRepository;
import com.note.user.domain.User;
import com.note.user.dto.UserDto;
import com.note.user.service.UserInfoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Package：com.note.user.service.impl
 * @Name：UserInfoImpl
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2023-12-17-19:38
 * @Description：用户信息获取实现
 */
@Service
@Slf4j
public class UserInfoServiceImpl implements UserInfoService {

    @Autowired
    private UserInfoRepository userInfoRepository;

    @Autowired
    private UserLoginRepository userLoginRepository;

    @Override
    public int validateUser(UserDto userDto) {
        User user = userLoginRepository.selectUser(userDto);
        //用户不存在
        if (user == null) {
            return ResponseCodeConstants.USER_NOT_FOUND;
        }
        //封禁用户
        if (user.getStatus() == 0) {
            return ResponseCodeConstants.USER_BANNED;
        }
        return ResponseCodeConstants.LOGIN_SUCCESS;
    }

    @Override
    public User getUserIndo(UserDto userDto) {
        return userInfoRepository.getUserInfo(userDto);
    }
}
