package com.note.user.service;

import com.note.user.domain.User;
import com.note.user.dto.UserDto;

/**
 * @Package：com.note.user.service
 * @Name：UserInfoService
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2023-12-17-19:38
 * @Description：用户信息获取服务接口定义
 */
public interface UserInfoService {
    /**
     * 用户获取信息验证
     *
     * @param userDto 用户登录Params模型
     * @return int
     */
    int validateUser(UserDto userDto);
    /**
     * 获取用户信息
     *
     * @param userDto 用户名
     * @return User
     */
    User getUserIndo(UserDto userDto);
}
