package com.note.user.service;

import com.note.user.dto.UserDto;

/**
 * @Package：com.note.user.service
 * @Name：TokenService
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2023-12-12-18:14
 * @Description：用户令牌操作接口
 */

public interface TokenService {
    /**
     * 获取用户Token
     *
     * @param userDto 用户登录Params模型
     * @return string
     */
    String getToken(UserDto userDto);

    /**
     * 设置用户Token
     *
     * @param userDto 用户登录Params模型
     * @param token
     */
    void setToken(UserDto userDto, String token);

    /**
     * 生产一个Token
     *
     * @param userDto 用户登录Params模型v
     * @return string
     */
    String generateToken(UserDto userDto);
}

