package com.note.user.service.impl;

import com.note.user.dao.TokenRepository;
import com.note.user.dto.UserDto;
import com.note.user.service.TokenService;
import com.note.user.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Package：com.note.user.service.impl
 * @Name：TokenServiceImpl
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2023-12-12-18:25
 * @Description：用户令牌的实现
 */
@Service
public class TokenServiceImpl implements TokenService {

    @Autowired
    private TokenRepository tokenRepository;

    /*获取用户Token*/
    @Override
    public String getToken(UserDto userDto) {
        /*加密密码*/
        userDto.setPassword(userDto.getPassword());
        return tokenRepository.getToken(userDto);
    }

    /*更新用户Token*/
    @Override
    public void setToken(UserDto userDto, String token) {
        /*更新令牌*/
        int status = tokenRepository.updateToken(userDto.getUsername(), token);
    }

    /*生成Token*/
    @Override
    public String generateToken(UserDto userDto) {
        //返回新的TOKEN（只获取，不更新）
        return JwtUtil.generateToken(userDto);
    }
}
