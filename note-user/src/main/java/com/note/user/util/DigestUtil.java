package com.note.user.util;

import cn.hutool.crypto.digest.MD5;
import org.springframework.util.StringUtils;

import java.nio.charset.StandardCharsets;

/**
 * @Package：com.note.user.util
 * @Name：DigestUtil
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2023-12-12-10:29
 * @Description：摘要加密工具类
 */
public class DigestUtil {
    private static final String SALT = "";/*随机生成一个ID作为盐*/
    ;  // 加盐
    private static final int INDEX = 0;    // 加盐位置
    private static final int COUNT = 2;    // 摘要次数

    public static MD5 getDigester() {

        /*
        Digester md5 = new Digester(DigestAlgorithm.MD5);
        //加盐
        md5.setSalt(SALT.getBytes(StandardCharsets.UTF_8));
        md5.setSaltPosition(INDEX);
        md5.setDigestCount(COUNT);
        */

        return new MD5(SALT.getBytes(StandardCharsets.UTF_8), INDEX, COUNT);
    }

    /**
     * 使用MD5算法对字符串进行加密
     *
     * @param value 待加密子字符串
     * @return 加密后的十六进制字符串
     */
    public static String encryptMd5hex(String value) {
        return getDigester().digestHex(value);
    }

    /**
     * 使用MD5算法对字符串进行验证
     *
     * @param value        待验证字符串
     * @param encryptValue 已加密字符串
     * @return boolean
     */
    public static boolean verifyMd5(String value, String encryptValue) {
        if (!StringUtils.hasLength(value) && !StringUtils.hasLength(encryptValue)) {
            return false;
        }
        String encryptedValue = encryptMd5hex(value);
        return encryptedValue.equals(encryptValue);
    }
}
