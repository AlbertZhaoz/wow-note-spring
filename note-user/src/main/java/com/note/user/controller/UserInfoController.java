package com.note.user.controller;

import com.note.user.constants.ResponseCodeConstants;
import com.note.user.dto.UserDto;
import com.note.user.enums.ResponseEnum;
import com.note.user.service.UserInfoService;
import com.note.user.vo.ResponseVo;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.constraints.NotBlank;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @Package：com.note.user.controller
 * @Name：UserInfoController
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2023-12-17-19:52
 * @Description：用户信息获取
 */
@Slf4j
@Validated
@RestController
@RequestMapping("/user")
@Tag(name = "UserInfoController", description = "用户个人信息获取接口")
public class UserInfoController {

    @Autowired
    private UserInfoService userInfoService;

    @ResponseBody
    @GetMapping("/info")
    @Operation(summary = "info", description = "获取用户信息接口（username）")
    public ResponseVo getUserInfo(@RequestParam @NotBlank(message = "用户名不能为空") String username) {
        UserDto userDto = new UserDto();
        userDto.setUsername(username);
        int validateUserCode = userInfoService.validateUser(userDto);
        return switch (validateUserCode) {
            case ResponseCodeConstants.USER_NOT_FOUND -> new ResponseVo(ResponseEnum.USER_NOT_FOUND);
            case ResponseCodeConstants.USER_BANNED -> new ResponseVo(ResponseEnum.USER_BANNED);
            case ResponseCodeConstants.LOGIN_SUCCESS ->
                    new ResponseVo(ResponseEnum.SUCCESS, userInfoService.getUserIndo(userDto));
            default -> new ResponseVo(ResponseEnum.FAIL);
        };
    }
}
