package com.note.user.controller;

import cn.hutool.json.JSONUtil;
import com.note.user.constants.ResponseCodeConstants;
import com.note.user.dto.UserDto;
import com.note.user.enums.ResponseEnum;
import com.note.user.service.UserRegisterService;
import com.note.user.vo.ResponseVo;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @Package：com.note.user.controller
 * @Name：UserRegisterController
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2023-12-11-11:06
 * @Description：用户注册控制类
 */
@Slf4j
@RestController
@RequestMapping("/user")
@Tag(name = "UserRegisterController", description = "用户注册类相关接口")
public class UserRegisterController {

    @Autowired
    private UserRegisterService userRegisterService;

    @ResponseBody
    @PostMapping("/register")
    @Operation(summary = "register", description = "用户注册接口 （用户名username|密码password|邮箱email）")
    public ResponseVo userRegister(@RequestBody @Validated({UserRegisterService.class}) UserDto userDto) {
        log.error(JSONUtil.toJsonStr(userDto));
        int statusCode = userRegisterService.registerUser(userDto);/*注册状态*/
        return switch (statusCode) {
            case ResponseCodeConstants.USERNAME_ALREADY_EXISTS -> new ResponseVo(ResponseEnum.USERNAME_ALREADY_EXISTS);
            case ResponseCodeConstants.REGISTER_FAIL -> new ResponseVo(ResponseEnum.REGISTER_FAIL);
            case ResponseCodeConstants.REGISTER_SUCCESS -> new ResponseVo(ResponseEnum.REGISTER_SUCCESS);
            default -> new ResponseVo(ResponseEnum.FAIL);
        };
    }
}
