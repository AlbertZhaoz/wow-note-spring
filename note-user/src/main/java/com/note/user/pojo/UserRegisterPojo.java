package com.note.user.pojo;

import cn.hutool.core.date.DateTime;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Package：com.note.user.pojo
 * @Name：UserRegisterPojo
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2023-12-11-17:28
 * @Description：用户注册时插入的数据模型
 */
@Data
@NoArgsConstructor
public class UserRegisterPojo {
    private String username;
    private String password;
    private String email;
    private int state;
    @SuppressWarnings("AlibabaLowerCamelCaseVariableNaming")
    private DateTime create_time;
    private String token;
}
