package com.note.user.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @Package：com.note.user.pojo
 * @Name：UserUpdateToken
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2023-12-12-19:49
 * @Description：用户更新令牌数据模型
 */
@Data
@AllArgsConstructor
public class UserUpdateToken {
    private String username;
    private String password;
    private String token;
}
