package com.note.user.dao.impl;

import com.note.user.dao.UserRegisterRepository;
import com.note.user.dao.mapper.UserRegisterMapper;
import com.note.user.pojo.UserRegisterPojo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @Package：com.note.user.dao.impl
 * @Name：UserRegisterRepositoryImpl
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2023-12-11-11:41
 * @Description：注册对表操作实现
 */
@Repository
public class UserRegisterRepositoryImpl implements UserRegisterRepository {
    @Autowired
    private UserRegisterMapper userRegisterMapper;

    /*保存用户*/
    @Override

    public int saveUser(UserRegisterPojo userRegisterPojo) {
        return userRegisterMapper.saveUser(userRegisterPojo);
    }
}
