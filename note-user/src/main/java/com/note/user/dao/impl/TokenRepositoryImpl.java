package com.note.user.dao.impl;

import com.note.user.dao.TokenRepository;
import com.note.user.dao.mapper.TokenMapper;
import com.note.user.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @Package：com.note.user.dao.impl
 * @Name：TokenRepositoryImpl
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2023-12-12-18:29
 * @Description：令牌操作处理实现
 */
@Repository
public class TokenRepositoryImpl implements TokenRepository {
    @Autowired
    private TokenMapper tokenMapper;

    /*更新令牌*/
    @Override
    public int updateToken(String username, String token) {
        return tokenMapper.updateToken(username, token);
    }

    /*获取令牌*/
    @Override
    public String getToken(UserDto userDto) {
        return tokenMapper.getToken(userDto);
    }
}
