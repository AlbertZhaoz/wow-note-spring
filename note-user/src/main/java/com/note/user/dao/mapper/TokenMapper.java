package com.note.user.dao.mapper;

import com.note.user.dto.UserDto;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Package：com.note.user.dao.mapper
 * @Name：TokenMapper
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2023-12-12-18:34
 * @Description：MBP Token处理映射
 */
@Mapper
public interface TokenMapper {

    /**
     * 更新用户令牌
     *
     * @return
     */
    int updateToken(@Param("username") String username, @Param("token") String token);

    /**
     * 获取用户令牌
     *
     * @return
     */

    String getToken(UserDto userDto);
}
