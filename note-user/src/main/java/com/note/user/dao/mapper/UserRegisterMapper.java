package com.note.user.dao.mapper;

import com.note.user.pojo.UserRegisterPojo;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Package：com.note.user.dao.mapper
 * @Name：UserRegisterMapper
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2023-12-11-11:42
 * @Description：MBP 注册映射
 */
@Mapper
public interface UserRegisterMapper {
    /**
     * 插入用户
     *
     * @param pojo 用户注册params模型
     * @return int
     */
    int saveUser(UserRegisterPojo pojo);
}
