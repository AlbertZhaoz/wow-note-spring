package com.note.user.dao.impl;

import com.note.user.dao.UserInfoRepository;
import com.note.user.dao.mapper.UserInfoMapper;
import com.note.user.domain.User;
import com.note.user.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @Package：com.note.user.dao.impl
 * @Name：UserInfoRepositoryImpl
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2023-12-17-19:45
 * @Description：用户信息获取实现
 */
@Repository
public class UserInfoRepositoryImpl implements UserInfoRepository {
    @Autowired
    private UserInfoMapper userInfoMapper;

    /*获取用户数据*/
    @Override
    public User getUserInfo(UserDto userDto) {
        return userInfoMapper.getUserInfo(userDto);
    }
}
