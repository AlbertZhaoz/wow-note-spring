package com.note.user.dao;

import com.note.user.domain.User;
import com.note.user.dto.UserDto;

/**
 * @Package：com.note.user.dao
 * @Name：UserRepository
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2023-12-11-11:35
 * @Description：登录对用户表操作的接口定义
 */
public interface UserLoginRepository {
    /**
     * 用户查询
     *
     * @param userDto 用户登录params模型
     * @return User
     */
    User selectUser(UserDto userDto);
}
