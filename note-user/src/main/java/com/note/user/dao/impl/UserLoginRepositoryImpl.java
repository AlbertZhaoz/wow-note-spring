package com.note.user.dao.impl;

import com.note.user.dao.UserLoginRepository;
import com.note.user.dao.mapper.UserLoginMapper;
import com.note.user.domain.User;
import com.note.user.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * @Package：com.note.user.dao.impl
 * @Name：UserLoginRepositoryImpl
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2023-12-11-11:41
 * @Description：登录对表操作实现
 */
@Repository
public class UserLoginRepositoryImpl implements UserLoginRepository {
    @Autowired
    private UserLoginMapper userLoginMapper;

    /*查询用户*/
    @Override
    public User selectUser(UserDto userDto) {
        return userLoginMapper.selectUser(userDto);
    }
}
