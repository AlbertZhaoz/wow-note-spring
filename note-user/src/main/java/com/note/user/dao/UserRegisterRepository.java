package com.note.user.dao;

import com.note.user.pojo.UserRegisterPojo;

/**
 * @Package：com.note.user.dao
 * @Name：UserRegisterRepository
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2023-12-11-11:40
 * @Description：注册对用户表的操作接口定义
 */
public interface UserRegisterRepository {
    /**
     * 保存用户
     *
     * @param userRegisterPojo 用户注册params模型
     * @return int
     */
    int saveUser(UserRegisterPojo userRegisterPojo);
}
