package com.note.user.dao.mapper;

import com.note.user.domain.User;
import com.note.user.dto.UserDto;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Package：com.note.user.dao.mapper
 * @Name：UserInfoMapper
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2023-12-17-19:46
 * @Description：MBP 用户信息获取映射
 */

@Mapper
public interface UserInfoMapper {
    /**
     * 获取用户信息
     *
     * @param userDto 用户名
     * @return User
     */
    User getUserInfo(UserDto userDto);
}
