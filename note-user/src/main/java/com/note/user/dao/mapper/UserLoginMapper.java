package com.note.user.dao.mapper;

import com.note.user.domain.User;
import com.note.user.dto.UserDto;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Package：com.note.user.dao.mapper
 * @Name：UserLoginMapper
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2023-12-11-11:42
 * @Description：MBP 登录映射
 */
@Mapper
public interface UserLoginMapper {
    /**
     * 查询用户
     *
     * @param dto 用户登录params模型
     * @return User
     */
    User selectUser(UserDto dto);
}
