package com.note.user.dao;

import com.note.user.domain.User;
import com.note.user.dto.UserDto;

/**
 * @Package：com.note.user.dao
 * @Name：UserInfoRepository
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2023-12-17-19:43
 * @Description：用户信息获取
 */
public interface UserInfoRepository {
    /**
     * 获取用户信息获取
     *
     * @param userDto username
     * @return User
     */
    User getUserInfo(UserDto userDto);
}
