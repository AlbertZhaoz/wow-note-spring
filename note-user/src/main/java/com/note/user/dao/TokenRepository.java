package com.note.user.dao;

import com.note.user.dto.UserDto;

/**
 * @Package：com.note.user.dao
 * @Name：TokenRepository
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2023-12-12-18:27
 * @Description：用户令牌处理数据库接口
 */
public interface TokenRepository {
    /**
     * 更新用户令牌
     *
     * @param username
     * @param token
     * @return
     */
    int updateToken(String username, String token);

    /**
     * 获取用户令牌
     *
     * @param userDto
     * @return
     */

    String getToken(UserDto userDto);
}
