package com.note.user.constants;

/**
 * @Package：com.note.user.constants
 * @Name：ForumConstants
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2023-12-11-19:14
 * @Description：表字段默认值定义
 */
public class ForumDefaultConstants {
    /*用户注册 status（状态） 默认值*/
    public static final int USER_REGISTER_STATE = 1;
}
