package com.note.user.constants;

/**
 * @Package：com.note.user.constants
 * @Name：ResponseCodeConstants
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2023-12-12-9:38
 * @Description：内部服务间状态码定义
 */
public class ResponseCodeConstants {

    /*Exception异常*/
    public static final int ERROR = -1; // 出现异常

    /*参数校验*/
    public static final int BAD_REQUEST = 400; // 请求参数有误或缺失

    /*登录*/
    public static final int LOGIN_SUCCESS = 200; // 登录成功
    public static final int USER_NOT_FOUND = 404; // 用户未找到
    public static final int WRONG_PASSWORD = 401; // 密码错误
    public static final int USER_BANNED = 403; // 用户被禁止
    public static final int LOGIN_FAIL = 500; // 登录失败

    /*注册*/
    public static final int REGISTER_SUCCESS = 201; // 注册成功
    public static final int USERNAME_ALREADY_EXISTS = 409; // 注册失败
    public static final int REGISTER_FAIL = 500; // 注册失败


}
