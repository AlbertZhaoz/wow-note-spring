package com.note.user.constants;

/**
 * @Package：com.note.user.constants
 * @Name：ResponseConstants
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2023-12-11-19:16
 * @Description：统一返回状态描述定义
 */
public class ResponseMessageConstants {
    /*登录*/
    public static final String LOGIN_SUCCESS = "登录成功";
    public static final String USER_NOT_FOUND = "用户名不存在";
    public static final String WRONG_PASSWORD = "用户密码错误";
    public static final String USER_BANNED = "该用户已封禁";
    public static final String LOGIN_FAIL = "登录失败";

    /*注册*/
    public static final String REGISTRATION_SUCCESS = "注册成功";
    public static final String USERNAME_ALREADY_EXISTS = "用户名已存在";
    public static final String REGISTER_FAIL = "注册失败";

}
