package com.note.user.constants;

/**
 * @Package：com.note.user.constans
 * @Name：TableConstans
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2023-12-11-10:54
 * @Description：全局表名定义
 */
public class TableNameConstants {
    /*用户表*/
    public static final String TABLE_USER = "user";
}
