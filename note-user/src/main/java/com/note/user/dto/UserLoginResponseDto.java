package com.note.user.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @Package：com.note.user.pojo
 * @Name：UserLoginResponseDto
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2023-12-17-11:28
 * @Description：用户登录成功返回的数据模型
 */
@Data
@AllArgsConstructor
public class UserLoginResponseDto {
    /*令牌*/
    @Schema(name = "token", description = "用户令牌")
    private String token;
}
