package com.note.user.config;

import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

/**
 * @Package：com.note.user.config
 * @Name：CorsConfig
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2023-12-14-9:52
 * @Description：跨域配置类
 */
/*@Configuration --- 已经在前端实现*/
public class CorsConfig {
    /*跨域请求最大有效时常*/
    private static final long MAX_AGE = (long) (24 * 60 * 60);

    public CorsFilter corsFilter() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration corsConfiguration = new CorsConfiguration();
        //访问源地址
        corsConfiguration.addAllowedOrigin("*");
        //访问源请求头
        corsConfiguration.addAllowedHeader("*");
        //访问源请求方法
        corsConfiguration.addAllowedMethod("*");
        //最大有效时常
        corsConfiguration.setMaxAge(MAX_AGE);
        //配置跨域
        source.registerCorsConfiguration("/**", corsConfiguration);
        return new CorsFilter(source);
    }
}
