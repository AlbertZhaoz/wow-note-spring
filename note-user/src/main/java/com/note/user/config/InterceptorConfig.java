package com.note.user.config;

import com.note.config.constants.constants.GatewayConstants;
import com.note.user.interceptor.JWTInterceptor;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @Package：com.note.user.config
 * @Name：InterceptorConfig
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2023-12-12-17:01
 * @Description：拦截器配置 --- 已经在GateWay网关中心实现
 */
/*@Configuration --- 已经在GateWay网关中心实现*/
public class InterceptorConfig implements WebMvcConfigurer {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        /*定义过滤器*/
        registry.addInterceptor(new JWTInterceptor())
                /*需拦截入口*/
                .addPathPatterns(GatewayConstants.ADD_PATH_PATTERNS)
                /*无需拦截入口*/
                .excludePathPatterns(GatewayConstants.DONT_PATH_PATTERNS);
    }
}
