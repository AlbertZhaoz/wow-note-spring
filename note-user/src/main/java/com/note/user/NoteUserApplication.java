package com.note.user;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import com.note.config.constants.annotate.EnableGatewayFilter;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

/**
 * @Package：com.note.user
 * @Name：NoteUserApplication
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2023-12-11-11:22
 * @Description：Spring启动
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableGatewayFilter
@MapperScan(value = "com.note.user.dao.mapper")
public class NoteUserApplication {

    public static void main(String[] args) {
        SpringApplication.run(NoteUserApplication.class, args);
    }

    /*开启负载均衡*/
    @Bean
    @LoadBalanced
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    /*负载均衡策略*/
    @Bean
    public IRule randomRule() {
        return new RandomRule();
    }

}
