package com.note.user.enums;

import com.note.user.constants.ResponseCodeConstants;
import com.note.user.constants.ResponseMessageConstants;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;

/**
 * @Package：com.note.user.enums
 * @Name：ResponseEnum
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2023-12-11-19:09
 * @Description：统一返回枚举类
 */
@Getter
@Schema(name = "ResponseEnum", description = "用户模块统一返回枚举类")
public enum ResponseEnum {
    /*默认状态定义*/
    SUCCESS(200, true, "请求成功"),
    FAIL(400, false, "请求失败"),

    /*用户登录状态定义*/
    LOGIN_SUCCESS(ResponseCodeConstants.LOGIN_SUCCESS, true, ResponseMessageConstants.LOGIN_SUCCESS),
    USER_NOT_FOUND(ResponseCodeConstants.USER_NOT_FOUND, false, ResponseMessageConstants.USER_NOT_FOUND),
    WRONG_PASSWORD(ResponseCodeConstants.WRONG_PASSWORD, false, ResponseMessageConstants.WRONG_PASSWORD),
    USER_BANNED(ResponseCodeConstants.USER_BANNED, false, ResponseMessageConstants.USER_BANNED),
    LOGIN_FAIL(ResponseCodeConstants.LOGIN_FAIL, false, ResponseMessageConstants.LOGIN_FAIL),
    /*用户注册状态定义*/
    REGISTER_SUCCESS(ResponseCodeConstants.REGISTER_SUCCESS, true, ResponseMessageConstants.REGISTRATION_SUCCESS),
    USERNAME_ALREADY_EXISTS(ResponseCodeConstants.USERNAME_ALREADY_EXISTS, false, ResponseMessageConstants.USERNAME_ALREADY_EXISTS),
    REGISTER_FAIL(ResponseCodeConstants.REGISTER_FAIL, false, ResponseMessageConstants.REGISTER_FAIL);

    /*状态码（*）*/
    private final int code;
    /*请求状态（*）*/
    private final boolean status;
    /*状态描述（*）*/
    private final String message;

    ResponseEnum(int code, boolean status, String message) {
        this.code = code;
        this.status = status;
        this.message = message;
    }

}
