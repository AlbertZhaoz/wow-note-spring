# wow-note 个人云笔记

> 本仓库为后端仓库，需要了解前端仓库请🍧[https://gitee.com/uyevan/wow-note-react](https://gitee.com/uyevan/wow-note-react)   
>> 您也可以在线预览成品🎉[http://note.jfkj.xyz](http://note.jfkj.xyz)

#### 项目简介 👨‍💻

>  wow-note 个人云笔记项目是专门用来在线 **存储** 和 **分享** 个人笔记文档的软件；使用者可以快速地进行创建文件夹或笔记等操作，也可以对其进行 **分享** 操作，以便更多人查看与利用此分享者笔记。开发此软件的根本目的是对个人以前所学的技术进行一个综合的总结，无论使在以前在学校教过或自学过的相关技术进行一个打捞和评估；其次是本项目可能会作为我的毕设项目，虽然我写过很多项目（**UY云，UyClub工具箱，乐应软件库**...）但无论从技术层面，开发成本和难度上来看 wow-note 这项目都比这些小项目复杂得多。   
>  wow-note项目后端是基于 SpringCloud 微服务方案扩展；前端是基于 NextJS（基于React的服务端渲染框架） 框架进行扩展。开发流程就是 **设计，边编码边测试，部署**，虽然不能说特别牛x，但对于我个人而言从设计到现在意义非凡，毕竟现在满脑子各种离谱Bug，如果对你有帮助不妨给个Star。![项目简介](https://pan.losfer.cn/view.php/22ba8b1539bbe2f1d283594a402f1301.png#pic_center)    

项目架构 🪜   
> ![项目架构图](https://pan.losfer.cn/view.php/680a854047142eebf8a113304dc464fd.png#pic_center)    
 
服务列表 🛎️   
> ![服务列表](https://pan.losfer.cn/view.php/7171a1742315f4639f661cd6613dcfa0.png#pic_center)
#### 功能特性 🎉
> - JWT用户认证：跟大部分项目一样，我也选择了 **JWT** 用户认证策略，使用的技术是Auth0。对所有在 note-config 中配置的验证用户接口进行令牌验证以便提供安全服务。
> - Nacos服务注册中心：没有使用 **Service RegisTration** 是因为Nacos提供了更简洁更快速的上线方案，同时还提供了 **配置中心** ，通过把所有服务注册到Nacos服务中，以便在业务上使用服务之间通讯以及启动多个服务实现负载均衡。
> - Nacos配置中心：理由跟注册中心一样，所有没有使用 **Spring Cloud Config** ；通过Nacos配置中心可以无感刷新应用Application配置，无需重启服务等。
> - 数据存储：项目中使用到了 **MySQL，MongoDB，Redis** 等三个数据库，在不同的业务需求上使用了不同特性的数据库实现方案。如用户服务中使用到了MySQL与Redis；笔记服务中使用到了MongoDB；分享服务中使用到了Redis；虽然都是最基础的利用多个数据库，但在不同的业务上使用不同的数据库就能高效实现数据存储与操作。
#### 环境要求 ✅
**Java** [https://www.java.com](https://www.java.com)
> java version "19.0.2" 2023-01-17   
Java(TM) SE Runtime Environment (build 19.0.2+7-44)   
Java HotSpot(TM) 64-Bit Server VM (build 19.0.2+7-44, mixed mode, sharing)

**Maven** [https://maven.apache.org](https://maven.apache.org/download.cgi)

> Apache Maven 3.9.0   
OS name: "windows 11", version: "10.0", arch: "amd64", family: "windows"

**Nacos** [https://nacos.io](https://nacos.io/)
> Nacos 2.2.3

**MySQL** [https://www.mysql.com](https://www.mysql.com/cn/)
> +-----------+   
| VERSION() |   
+-----------+   
| 5.7.26    |   
+-----------+   

**MongoDB** [https://www.mongodb.com](https://www.mongodb.com/zh-cn)
>  "version": "6.0.5",   
    "allocator": "tcmalloc",   
    "distmod": "windows",   
    "distarch": "x86_64",   
    "target_arch": "x86_64"   

**Redis**  [https://redis.io](https://redis.io/)

> Redis server v=5.0.14.1    
> malloc=jemalloc-5.2.1-redis    
> bits=64   

**Docker** [https://www.docker.com](https://www.docker.com/)

> Cloud integration: v1.0.35+desktop.5   
 Version:           24.0.6   
 API version:       1.43   
 Go version:        go1.20.7   
 OS/Arch:           windows/amd64   

#### 技术栈 🍵
 - SpringCloud(GateWay)
 - SpringBoot
 - Nacos(Config,Registry)
 - MySQL
 - MyBatisPlus
 - MongoDB
 - Redis
 - Docker
 - JWT(Auth0)
#### 快速开始 🔛
>  我首先是在电脑（Windows11）本地进行开发，然后通过Docker打包镜像，再放到服务器进行部署的，可以通过拉取本地进行调试，也可以通过Docker进行拉去调试。

💻 本地拉去运行
> - 环境配置：配置本地 **Java,MySQL,MongoDB,Redis,Nacos** 环境；
> - 拉取项目：`git clone https://gitee.com/uyevan/wow-note-spring.git`；
> - 应用配置：进入每个应用模块的 **Application.yml** 更改相应的配置，如应用端口，Nacos注册配置，数据库配置与调试开关等等；**提示：你也可以选择在Nacos中创建一个服务配置**；
> - 运行项目：可以在IDEA中启动项目了；你也可以 `mvn clean package` 打包，再通过 `java -jar target/note-user.jar` 运行项目；启动前确保Nacos服务注册已经配置并且正常启动了Nacos服务；每个模块都需要单独打包启动；

🦣 拉去镜像运行
> - 环境配置：无论你是什么系统，要确保 **Docker** 环境并且正常启动；并且你要保证有足够空间，因为总共需要拉去五个镜像,因为国内无法访问DockerHub，所以我这里使用了阿里云镜像仓库服务（个人免费），无论哪个只要有Docker环境即可；
> - 拉去镜像：`docker pull registry.cn-shenzhen.aliyuncs.com/wow-note-spring/wow-note-note-user:latest`；
> - 运行镜像：`docker run -d -p 8081:8081 --name wow-note-user registry.cn-shenzhen.aliyuncs.com/wow-note-spring/wow-note-note-user:latest`；
> - 运行测试：可以调试一下相关的接口来验证是否正常启动；提示：这里我举例一个用户服务模块，您需要拉去你要运行的所有服务模块（笔记wow-`note-note-notes`/分享`wow-note-note-share`/网关`wow-note-note-gateway`）再运行前端，否则前端会报 **Error** 错误；

#### 项目结构 🗃️
🐝 如下是用户服务模块的主要结构，我分结构比较细，已于后期维护项目时好理解，具体可以看仓库；主要结构如下：
> └─note-user   
>     ├─src   
>     │  ├─main   
>     │  │  ├─java   
>     │  │  │  └─com   
>     │  │  │      └─note   
>     │  │  │          └─user   
>     │  │  │              ├─config   
>     │  │  │              ├─constants   
>     │  │  │              ├─controller   
>     │  │  │              ├─dao   
>     │  │  │              │  ├─impl   
>     │  │  │              │  └─mapper   
>     │  │  │              ├─domain   
>     │  │  │              ├─dto   
>     │  │  │              ├─enums   
>     │  │  │              ├─interceptor   
>     │  │  │              ├─pojo   
>     │  │  │              ├─service   
>     │  │  │              │  └─impl   
>     │  │  │              ├─util   
>     │  │  │              └─vo   
>     │  │  └─resources   
>     │  │      └─mapper   

🗃️ 其他模块根用户模块结构类似，这就不都放出来了；剩余模块如下：

> ├─note-tag  **标签服务模块**   
> ├─note-comment  **评论服务模块**   
> ├─note-config **统一配置中心**   
> ├─note-gateway  **网关中心**   
> ├─note-notes **笔记服务模块**   
> ├─note-share **分享服务模块**  
 
#### 项目预览 💻
> NextUI是支持全段自定义适配的，一下是电脑端的预览结果，手机端不分上下；
> ![主页](https://pan.losfer.cn/view.php/378d247e3a4a5f4df2ec9e6bfe222d28.png#pic_center)
> ![文档中心](https://pan.losfer.cn/view.php/a2d229d4d5592ebf123d5488f375662a.png#pic_center)
> ![笔记编辑](https://pan.losfer.cn/view.php/215ca0d889bb61a6905c97c568e5e477.png#pic_center)
> ![分享中心](https://pan.losfer.cn/view.php/df236eeea320f0eb4ec77ea7fc87f762.png#pic_center)
> ![分享查看](https://pan.losfer.cn/view.php/00ee13a00cfd9524337e5678b6b35234.png#pic_center)
#### 其他说明 🤥
> 本项目是我学习过程中写出的一个项目，写的项目也一般般，我的技术也一般般，刚好都一般般。如果对你有帮助可以给个 **Star** 噢！如果你觉得还有可优化的地方可以 Pull 到仓库，欢迎你来参与~