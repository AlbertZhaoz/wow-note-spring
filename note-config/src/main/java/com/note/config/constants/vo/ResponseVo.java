package com.note.config.constants.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.Data;

import java.io.Serializable;

/**
 * @Package：com.note.notes.vo
 * @Name：ResponseVo
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2023-12-23-10:02
 * @Description：全局统一返回类、
 */
@Data
@Tag(name = "ResponseVo", description = "统一返回处理")
public class ResponseVo<T> implements Serializable {
    @Schema(name = "code", description = "状态码")
    private int code;
    @Schema(name = "status", description = "请求状态")
    private boolean status;
    @Schema(name = "message", description = "状态说明")
    private String message;
    @Schema(name = "data", description = "状态数据")
    private T data;

    /*全参构造*/
    public ResponseVo(int code, boolean status, String message, T data) {
        this.code = code;
        this.status = status;
        this.message = message;
        this.data = data;
    }

    /*无data构造*/
    public ResponseVo(int code, boolean status, String message) {
        this.code = code;
        this.status = status;
        this.message = message;
        this.data = null;
    }
}
