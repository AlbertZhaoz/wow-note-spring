package com.note.config.constants.constants;

/**
 * @Package：com.note.user.constants
 * @Name：GateWayConstants
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2023-12-24-14:31
 * @Description：网关中心常量定义
 */
public class GatewayConstants {
    /*避免绕过网关，每个子模块检测key*/

    public static final String GATEWAY_KEY_NAME = "GatewayKey";
    public static final String GATEWAY_KEY = "wownote!E@W#A$N";

    /*全局白名单入口 [网关不验证+消费者不验证] 【这些都是无需提交Token的接口】 */
    public static final String[] DONT_PATH_PATTERNS = {
            "/test", "/user/user/login", "/user/user/register", "/share/view/**", "/note/note/viewShareNote/**"
    };
    /*需要检验用户状态的接口 [检验参数中的username] 【这些都必须提交username需要验证用户状态】*/
    public static final String[] ADD_PATH_PATTERNS = {
            "/user/user/info",
            "/note/folder/createFolder",
            "/note/folder/listFolders",
            "/note/note/recordNote",
            "/note/note/listNotes",
            "/note/note/viewNote",
            "/note/note/removeNote",
            "/note/note/updateNote",
            "/share/share/shares"
    };

    /*模块防止下游直接访问检验 【这些的路由所有配置filter的模块都不验证有没有走网关】*/
    public static final String[] SERVER_DONT_PATH_PATTERNS = {
            "/test"
    };
}
