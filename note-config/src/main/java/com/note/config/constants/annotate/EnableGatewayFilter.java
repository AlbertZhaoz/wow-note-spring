package com.note.config.constants.annotate;

import com.note.config.constants.filter.GatewayFilter;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @Package：com.note.gateway.annotation
 * @Name：GatewayFilter
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2023-12-24-14:44
 * @Description：全模块拦截器
 */

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Import({GatewayFilter.class})
@Inherited
public @interface EnableGatewayFilter {
}
