package com.note.config.constants.constants;

/**
 * @Package：com.note.config.constants.constants
 * @Name：GlobalConstants
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2024-02-03-0:25
 * @Description：全局常量参数
 */
public class GlobalConstants {
    /*系统错误*/
    public static final int ERROR = -1;
    public static final int FAIL = -1;
    /*参数校验*/
    public static final int BAD_REQUEST = 400;
}
