package com.note.config.constants.domain;

import cn.hutool.core.date.DateTime;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.MongoId;

/**
 * @Package：com.note.share.domain
 * @Name：Share
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2024-02-02-17:44
 * @Description：笔记或目录分享文档数据模型
 */
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "share")
public class Share {
    @Id
    @MongoId
    @Field(name = "_id")
    @Schema(description = "分享ID")
    private String id;
    @Field(name = "end_time")
    @Schema(description = "分享结束时间")
    private DateTime endTime;
    @Schema(description = "当前分享链接状态")
    private int type;
    /*如果是笔记分享 folder_id=0 ，反则相反；*/
    @Field(name = "folder_id")
    @Schema(description = "文件夹Id")
    private String folderId;
    @Field(name = "note_id")
    @Schema(description = "笔记Id")
    private String noteId;
    @JsonIgnore/*忽略此属性*/
    @Field(name = "user_id")
    @Schema(description = "用户名")
    private String userId;

    /*外部属性*/
    @Schema(description = "笔记名")
    private String title;
}
