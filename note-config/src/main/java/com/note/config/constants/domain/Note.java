package com.note.config.constants.domain;

import cn.hutool.core.date.DateTime;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * @Package：com.note.notes.domain
 * @Name：Note
 * @Author：热伊木
 * @Email：uyevan@163.com
 * @Date：2023-12-21-12:08
 * @Description：笔记数据模型
 */

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "note")
public class Note {
    @Id
    @Schema(description = "笔记ID")
    private String id;
    @Schema(description = "笔记短标题")
    private String title;
    @Schema(description = "笔记内容 MD格式")
    private String content;
    @Field(name = "create_time")
    @Schema(description = "创建时间")
    private DateTime createTime;
    @Field(name = "update_time")
    @Schema(description = "更新时间")
    private DateTime updateTime;
    @Field(name = "folder_id")
    @Schema(description = "目录ID 根笔记则null")
    private String folderId;
    @Field(name = "tag_id")
    @Schema(description = "标签ID")
    private String tagId;
    @Field(name = "user_id")
    @Schema(description = "用户名")
    private String userId;
    /*分享文档*/
    @Schema(description = "分享ID")
    private String shareId;
    @Schema(description = "分享状态")
    private int type;
}
